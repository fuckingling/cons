﻿namespace Lynn.Infastructure.Repository
{
    public enum DataBaseType
    {
        SqlServer = 0,
        MySql = 1,
        SqlLite = 2
    }
}
