﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Lynn.Infastructure.Repository
{
    [AppIgnore]
    public interface IRepository<T> where T : class
    {
        Task<bool> Instert(T entity);
        Task<bool> Instert(IEnumerable<T> entities);
        Task<bool> Delete(T entity);
        Task<bool> Delete(IEnumerable<T> entities);
        Task<bool> Update(T entity);
        Task<bool> Update(IEnumerable<T> entities);
        /// <summary>
        /// 根据条件批量更新，注意这里批量也是单条，不允许出现 update xxx where xxx;
        /// </summary>
        /// <param name="where">筛选条件</param>
        /// <param name="param">需要更新的字段和值</param>
        /// <returns></returns>
        Task<bool> Update(Expression<Func<T, bool>> where, object param);
        Task<IList<T>> GetList(Expression<Func<T, bool>> where);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="page">当前页码 从1开始</param>
        /// <param name="pageSize">每页条数</param>
        /// <param name="where">查询条件</param>
        /// <param name="order">排序字段</param>
        /// <param name="orderByasc">排序方式 默认升序</param>
        /// <param name="totalPage">总页码</param>
        /// <param name="totalCount">总条数</param>
        /// <returns></returns>
        IList<T> GetListPage(int page, int pageSize, out int totalPage, out int totalCount, Expression<Func<T, bool>> where, Expression<Func<T, object>> order, bool orderByasc = true);
    }
}
