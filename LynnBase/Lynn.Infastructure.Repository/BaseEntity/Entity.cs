﻿
using Dapper.Contrib.Extensions;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lynn.Infastructure.Repository.BaseEntity
{
    public class Entity: IUpdateTime
    {
        /// <summary>
        /// ID
        /// </summary>
        [System.ComponentModel.DataAnnotations.Key]
        public long Id { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;
        public DateTime UpdateTime { get; set; } = DateTime.Now;
    }
}
