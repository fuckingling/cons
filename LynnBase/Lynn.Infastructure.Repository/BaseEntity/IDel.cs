﻿
using Lynn.Infastructure.Repository.EFCore;

namespace Lynn.Infastructure.Repository.BaseEntity
{
    public interface IDel
    {
        /// <summary>
        /// 是否删除
        /// </summary>
        [Length("逻辑删除")]
        public bool Del { get; set; }
    }
}
