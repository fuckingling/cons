﻿namespace Lynn.Infastructure.Repository.BaseEntity
{
    public interface IRowVersion
    {
        /// <summary>
        /// 乐观锁
        /// </summary>
        public int RowVersion { get; set; }
    }
}
