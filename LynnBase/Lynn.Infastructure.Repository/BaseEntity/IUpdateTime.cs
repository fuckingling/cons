﻿using System;

namespace Lynn.Infastructure.Repository.BaseEntity
{
    public interface IUpdateTime
    {
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; }
    }
}
