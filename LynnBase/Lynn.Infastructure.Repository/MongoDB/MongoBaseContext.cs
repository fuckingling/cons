﻿using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynn.Infastructure.Repository.MongoDB
{
    [AppIgnore]
    public class MongoBaseContext
    {
        public IMongoClient Client { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
