﻿using Lynn.Infastructure.Repository.BaseEntity;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lynn.Infastructure.Repository.MongoDB
{
    [AppIgnore]
    public interface IMongoBaseRepository<T> : IRepository<T> where T : Entity
    {
        IQueryable<T> Query(Expression<Func<T, bool>> where);
    }
}
