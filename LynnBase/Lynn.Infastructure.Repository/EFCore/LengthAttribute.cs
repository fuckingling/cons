﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynn.Infastructure.Repository.EFCore
{
    /// <summary>
    /// 字段长度
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class LengthAttribute : Attribute
    {
        /// <summary>
        /// 字段名称
        /// </summary>
        public string Comment { get; private set; }
        /// <summary>
        /// 长度
        /// </summary>
        public int Precision { get; private set; }
        /// <summary>
        /// 精度
        /// </summary>
        public int Scal { get; private set; }
        /// <summary>
        /// 字段长度限制
        /// </summary>
        /// <param name="comment">字段名称</param>
        /// <param name="precision">长度</param>
        /// <param name="scal">小数精度</param>
        public LengthAttribute(string comment, int precision, int scal = 0)
        {
            Comment = comment;
            Precision = precision;
            Scal = scal;
        }
        /// <summary>
        /// 字段名称
        /// </summary>
        /// <param name="comment"></param>
        public LengthAttribute(string comment)
        {
            Comment = comment;
        }
    }
}
