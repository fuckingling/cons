﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Lynn.Infastructure.Repository.EFCore
{
    [AppIgnore]
    public interface IEFCoreBaseRepository<T> : IRepository<T> where T : class
    {
        Task<bool> Commit();
        void InstertByCommit(T entity);
        void DeleteByCommit(T entity);
        void DeleteByCommit(IEnumerable<T> entities);
        void UpdateByCommit(T entity);
        IQueryable<T> Query(Expression<Func<T, bool>> where);
        IQueryable<T> QueryIgnoreLogDelete(Expression<Func<T, bool>> where);
        T QueryFirst(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includes);
        /// <summary>
        /// 连表查询
        /// </summary>
        /// <param name="where"></param>
        /// <param name="includes">关联对象</param>
        /// <returns></returns>
        IQueryable<T> Query(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includes);
    }
}
