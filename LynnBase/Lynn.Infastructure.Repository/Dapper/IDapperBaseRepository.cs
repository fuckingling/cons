﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Lynn.Infastructure.Repository.Dapper
{
    [AppIgnore]
    public interface IDapperBaseRepository
    {
        /// <summary>
        /// 执行语句
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task<int> ExecuteAsync(string sql, object param = null, IDbTransaction transaction = null);
        /// <summary>
        /// 查询获取的第一行的第一列
        /// </summary>
        /// <typeparam name="Entity"></typeparam>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task<TResult> ExecuteScalarAsync<TResult>(string sql, object param = null, IDbTransaction transaction = null);
        /// <summary>
        /// 查询语句
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="param"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task<IList<TResult>> QueryAsync<TResult>(string sql, object param = null, IDbTransaction transaction = null);
        /// <summary>
        /// 事务
        /// </summary>
        /// <param name="il">默认重复读</param>
        /// <returns></returns>
        IDbTransaction BeginTransaction(IsolationLevel il = IsolationLevel.RepeatableRead);
        Task<bool> Instert<T>(T entity) where T : class;
        Task<bool> Instert<T>(IEnumerable<T> entities) where T : class;
        Task<bool> Delete<T>(T entity) where T : class;
        Task<bool> Delete<T>(IEnumerable<T> entities) where T : class;
        Task<bool> Update<T>(T entity) where T : class;
        Task<bool> Update<T>(IEnumerable<T> entities) where T : class;
        /// <summary>
        /// 根据条件批量更新，注意这里批量也是单条，不允许出现 update xxx where xxx;
        /// </summary>
        /// <param name="where">筛选条件</param>
        /// <param name="param">需要更新的字段和值</param>
        /// <returns></returns>
        Task<bool> Update<T>(Expression<Func<T, bool>> where, object param) where T : class;
        Task<IList<T>> GetList<T>(Expression<Func<T, bool>> where) where T : class;

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="page">当前页码 从1开始</param>
        /// <param name="pageSize">每页条数</param>
        /// <param name="where">查询条件</param>
        /// <param name="order">排序字段</param>
        /// <param name="orderByasc">排序方式 默认升序</param>
        /// <param name="totalPage">总页码</param>
        /// <param name="totalCount">总条数</param>
        /// <returns></returns>
        IList<T> GetListPage<T>(int page, int pageSize, out int totalPage, out int totalCount, Expression<Func<T, bool>> where, Expression<Func<T, object>> order, bool orderByasc = true) where T : class;
    }
}
