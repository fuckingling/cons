﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Lynn.Infastructure.Extension
{
    public static class AttributeExtension
    {
        public static TResult GetAttribute<TResult>(this PropertyInfo value) where TResult : Attribute
        {
            return (TResult)value.GetCustomAttribute(typeof(TResult));
        }
    }
}
