﻿using System;

namespace Lynn.Infastructure.Extension
{
    public static class DateTimeExtension
    {
        /// <summary>
        /// UTC转当前时区
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime ToCurrentTimeZone(this DateTime dateTime)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(dateTime, TimeZoneInfo.Local);
        }
        /// <summary>
        /// 取一天的开始时间
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime ToBeiginTimeOfDay(this DateTime dateTime)
        {
            return Convert.ToDateTime(dateTime.ToString("yyyy-MM-dd 00:00:00"));
        }
        /// <summary>
        /// 取一个月的开始时间
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime ToBeiginTimeOfMonth(this DateTime dateTime)
        {
            return Convert.ToDateTime(dateTime.ToString("yyyy-MM-01 00:00:00"));
        }
        /// <summary>
        /// 取一年的开始时间
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime ToBeiginTimeOfYear(this DateTime dateTime)
        {
            return Convert.ToDateTime(dateTime.ToString("yyyy-01-01 00:00:00"));
        }
        /// <summary>
        /// unix时间戳 毫秒
        /// </summary>
        /// <param name="time">当前时间</param>
        /// <returns></returns>
        public static long ToUnix(this DateTime time)
        {
            return (time.ToUniversalTime().Ticks - 621355968000000000) / 10000;
        }
    }
}
