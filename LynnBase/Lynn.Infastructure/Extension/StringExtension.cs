﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lynn.Infastructure.Extension
{
    public static class StringExtension
    {
        public static int ToInt(this string value)
        {
            return Convert.ToInt32(value);
        }
        public static TEnum ToEnum<TEnum>(this string value)
        {
            return (TEnum)Enum.Parse(typeof(TEnum), value);
        }
        public static DateTime ToDateTime(this string value)
        {
            return Convert.ToDateTime(value);
        }
        public static bool IsNull(this string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }
    }
}
