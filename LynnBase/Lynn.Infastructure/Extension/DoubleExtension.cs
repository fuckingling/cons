﻿using System;

namespace Lynn.Infastructure.Extension
{
    public static class DoubleExtension
    {
        /// <summary>
        /// 四舍五入
        /// </summary>
        /// <param name="d"></param>
        /// <param name="len">小数长度</param>
        /// <returns></returns>
        public static double Round(this double d, int len = 2)
        {
            return Math.Round(d, len);
        }
        public static decimal Round(this decimal d, int len = 2)
        {
            return Math.Round(d, len);
        }
    }
}
