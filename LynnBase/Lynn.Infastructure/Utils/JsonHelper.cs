using System;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;

namespace Lynn.Infastructure.Utils
{
    /// <summary>
    /// Json序列/反序列化 直接依赖System.Text.Json
    /// </summary>
    public static class JsonHelper
    {
        public static JsonSerializerOptions options { get; private set; }
        static JsonHelper()
        {
            options = new JsonSerializerOptions()
            {
                //从string里获取数字
                NumberHandling = JsonNumberHandling.AllowReadingFromString,
                //忽略空
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
                //格式化
                Converters = { new JsonStringEnumConverter(), new DateTimeConverter(), new DateTimeNullableConverter() },
                //设置支持中文的unicode编码
                Encoder = JavaScriptEncoder.Create(UnicodeRanges.All),
                
                //启用驼峰格式
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                //启用缩进设置
                WriteIndented = true,
                //最深3层
                MaxDepth = 10
            };
        }
        /// <summary>
        /// 将model序列号成json
        /// </summary>
        /// <param name="t">model</param>
        /// <returns></returns>
        public static string ObjectToJson<TValue>(TValue t)
        {
            return JsonSerializer.Serialize(t, options);
        }
        /// <summary>
        /// 将json反序列化成model
        /// </summary>
        /// <param name="json">json</param>
        /// <returns></returns>
        public static TValue JsonToObject<TValue>(string json)
        {
            return JsonSerializer.Deserialize<TValue>(json, options);
        }
        public static object JsonToObject(string json, Type returnType)
        {
            return JsonSerializer.Deserialize(json, returnType, options);
        }
    }
    /// <summary>
    /// 时间转换
    /// </summary>
    public class DateTimeConverter : JsonConverter<DateTime>
    {
        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            return DateTime.Parse(reader.GetString());
        }
        public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
        {
            writer.WriteStringValue(value.ToString("yyyy-MM-dd HH:mm:ss"));
        }
    }
    /// <summary>
    /// 时间转换 可空属性时候
    /// </summary>
    public class DateTimeNullableConverter : JsonConverter<DateTime?>
    {
        public override DateTime? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            return string.IsNullOrEmpty(reader.GetString()) ? default(DateTime?) : DateTime.Parse(reader.GetString());
        }
        public override void Write(Utf8JsonWriter writer, DateTime? value, JsonSerializerOptions options)
        {
            writer.WriteStringValue(value?.ToString("yyyy-MM-dd HH:mm:ss"));
        }
    }
}