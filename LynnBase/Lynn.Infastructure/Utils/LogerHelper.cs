﻿
using NLog;
using System;

namespace Lynn.Infastructure.Utils
{
    public class LogerHelper
    {
        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();
        public static void Error(string err)
        {
            logger.Error(err);
        }
        public static void Error(string title, Exception ex)
        {
            while (ex != null)
            {
                logger.Error(ex, title);
                ex = ex.InnerException;
            }
        }
        public static void Warn(string err)
        {
            logger.Warn(err);
        }
        public static void Debug(string err)
        {
            logger.Debug(err);
        }
        public static void Info(string err)
        {
            logger.Info(err);
        }
        public static void Fatal(string err)
        {
            logger.Fatal(err);
        }
    }
}
