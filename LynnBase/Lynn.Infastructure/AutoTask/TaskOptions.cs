﻿
using System;

namespace Lynn.Infastructure.AutoTask
{
    public class TaskOptions
    {
        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 六位Cron表达式秒 分 时 日 月 周 
        /// </summary>
        public string Cron { get; set; }
        /// <summary>
        /// 是否滑动执行，假如任务执行时间是每天1点、2点、3点，1点的时候任务执行了一个半小时；滑动执行则是：1点任务执行完成立马执行两点的任务；非滑动则是跳过2点的任务3点再执行
        /// </summary>
        public bool Sliding { get; set; } = true;
        /// <summary>
        /// 每年执行次数 0不限制
        /// </summary>
        public int Times { get; set; } = 0;

    }
}
