﻿using Microsoft.Extensions.Configuration;
using Lynn.Infastructure.AutoTask;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class TaskService
    {
        public static IServiceCollection AddAutoTasks(this IServiceCollection services, IConfiguration config)
        {
            services.AddSingleton(typeof(TaskConfig), new TaskConfig(services, config));
            return services;
        }
    }
}
