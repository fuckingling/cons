﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynn.Infastructure.Aop.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class NotWrapResultAttribute : Attribute
    {
    }
}
