﻿
namespace Lynn.Infastructure.Aop.Jwt
{
    public class JwtOption
    {
        /// <summary>
        /// Token是谁颁发的
        /// </summary>
        public string Issuer { get; set; }
        /// <summary>
        /// Token给那些客户端去使用
        /// </summary>
        public string Audience { get; set; }
        /// <summary>
        /// 用于加密的key 必须是16个字符以上，要大于128个字节
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// 过期时间 单位：秒
        /// </summary>
        public int ExpireSeconds { get; set; }
    }
}
