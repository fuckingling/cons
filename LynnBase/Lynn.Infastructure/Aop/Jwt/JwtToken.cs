﻿namespace Lynn.Infastructure.Aop.Jwt
{
    public class JwtToken
    {
        /// <summary>
        /// Token类型
        /// </summary>
        public string Type { get; set; } = "jwt";
        /// <summary>
        /// 过期时间 单位：秒
        /// </summary>
        public int Exp { get; set; }
        /// <summary>
        /// 登录票据
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// 携带的信息
        /// </summary>
        public object Info { get; set; }
    }
}
