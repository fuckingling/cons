﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynn.Infastructure.Aop.Model
{
    /// <summary>
    /// 错误类型
    /// </summary>
    public enum ErrorType
    {
        常规错误 = 500,
        尚未登录 = 405,
    }
}
