﻿namespace Lynn.Infastructure.Aop.Model
{
    public class Msg
    {
        public Msg()
        {
            this.Message = "";
            this.Code = 0;
        }
        public Msg(string err)
        {
            this.Message = err;
            this.Code = 1;
        }
        /// <summary>
        /// 返回代码 0表示无错
        /// </summary>
        public int Code { get; set; }
        /// <summary>
        /// 错误消息
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 返回结果
        /// </summary>
        public object Data { get; set; }
    }
}
