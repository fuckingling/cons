﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynn.Infastructure.Aop.Model
{
    public class Error:Exception
    {
        public int code;
        public Error()
        {
            code = 1;
        }
        public Error(string message):base(message)
        {
            code = 1;
        }
        public Error(string message,int code) : base(message)
        {
            this.code = code;
        }
        public Error(string message, Exception innerException):base(message,innerException)
        {

        }
    }
}
