﻿using IdentityModel.Client;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Lynn.Infastructure.Net.Http
{
    public class HttpOption
    {
        private IdentityInfo _identity;
        private SSL _ssl;
        /// <summary>
        /// 超时时间 毫秒
        /// </summary>
        public int TimeOut { get; set; } = 2000;
        /// <summary>
        /// 重试次数
        /// </summary>
        public int ReTry { get; set; } = 1;
        public string DefaultName { get; set; } = "default";
        public void UseSSL(Action<SSL> action)
        {
            _ssl = new SSL();
            action.Invoke(_ssl);
        }
        public void UseAuthorize(Action<IdentityInfo> action)
        {
            _identity = new IdentityInfo();
            action.Invoke(_identity);
        }
        internal SSL SSL { get => _ssl; }
        internal IdentityInfo Identity
        {
            get => _identity;
        }
    }

}

