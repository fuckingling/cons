﻿using MQTTnet;
using MQTTnet.Client.Connecting;
using MQTTnet.Client.Disconnecting;
using MQTTnet.Extensions.ManagedClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lynn.Infastructure.Net.MQTT
{
    public abstract class MqttHandleAbstract
    {
        internal IManagedMqttClient ManagedMqttClient { get; set; }
        public abstract Task Connected(MqttClientConnectedEventArgs args);
        public abstract Task Disconnected(MqttClientDisconnectedEventArgs args);
        public abstract Task<bool> MessageReceived(string topic, string msg, string responseTopic);
        public async Task PublishAsync(string topic, string msg, string responseTopic = null)
        {
            var message = new MqttApplicationMessageBuilder().WithTopic(topic).WithPayload(msg);
            if (!string.IsNullOrWhiteSpace(responseTopic)) message.WithResponseTopic(responseTopic);
            await ManagedMqttClient.InternalClient.PublishAsync(message.Build());
        }
    }
}
