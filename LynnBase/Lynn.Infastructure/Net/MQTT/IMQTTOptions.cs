﻿using MQTTnet.Protocol;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynn.Infastructure.Net.MQTT
{
    public interface IMQTTOptions
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string ClientId { get; set; }
        public string Server { get; set; }
        public string Topic { get; set; }
        public int? Port { get; set; }
        public MqttQualityOfServiceLevel Level => MqttQualityOfServiceLevel.AtLeastOnce;
        public void WithCredentials(string name, string pass);
        public void WithTcpServer(string server, int? port = null);
        public void WithClientId(string clientId);
    }
}
