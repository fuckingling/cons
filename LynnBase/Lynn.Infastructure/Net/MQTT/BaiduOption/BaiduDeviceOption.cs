﻿using MQTTnet.Protocol;
using Lynn.Infastructure.Extension;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynn.Infastructure.Net.MQTT.BaiduOption
{
    public class BaiduDeviceOption: IMQTTOptions
    {
        public string IoTCoreId { get; set; }
        public string DeviceKey { get; set; }
        public string DeviceSecret { get; set; }
        public MqttQualityOfServiceLevel Level => MqttQualityOfServiceLevel.AtLeastOnce;
        public string UserName { get => $"thingidp@{IoTCoreId}|{DeviceKey}|0|MD5"; set { } }
        public string PassWord { get => $"{DeviceKey}&0&MD5{DeviceSecret}".ToMD5(); set { } }
        public string ClientId { get => $"{IoTCoreId}{DeviceKey}"; set { } }
        public string Server { get; set; }
        public int? Port { get; set; }
        public string Topic { get; set; }

        public void WithClientId(string clientId)
        {
            this.IoTCoreId = clientId;
        }

        public void WithCredentials(string name, string pass)
        {
            this.DeviceKey = name;
            this.DeviceSecret = pass;
        }
        public void WithTcpServer(string server, int? port = null)
        {
            this.Server = server;
            this.Port = port;
        }
    }
}
