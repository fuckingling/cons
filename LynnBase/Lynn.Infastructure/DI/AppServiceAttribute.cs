﻿using System;

namespace Microsoft.Extensions.DependencyInjection
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class AppServiceAttribute: Attribute
    {
        public AppServiceAttribute(AppServiceTargets autoFacTargets)
        {
            this.autoFacTargets = autoFacTargets;
        }
        public AppServiceTargets autoFacTargets { get; private set; }
    }
}
