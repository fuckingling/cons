﻿using System;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// 不注入
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class AppIgnoreAttribute : Attribute
    {
    }
}
