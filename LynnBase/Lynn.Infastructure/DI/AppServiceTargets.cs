﻿namespace Microsoft.Extensions.DependencyInjection
{
    public enum AppServiceTargets
    {
        /// <summary>
        /// 瞬态
        /// </summary>
        Transient = 0,
        /// <summary>
        /// 作用域
        /// </summary>
        Scoped,
        /// <summary>
        /// 单例
        /// </summary>
        Singleton
    }
}
