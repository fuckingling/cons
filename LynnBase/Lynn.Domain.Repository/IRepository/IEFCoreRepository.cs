﻿using Microsoft.Extensions.DependencyInjection;
using Lynn.Infastructure.Repository;
using Lynn.Infastructure.Repository.EFCore;

namespace Lynn.Domain.IRepository
{
    public interface IEFCoreRepository<T> : IEFCoreBaseRepository<T> where T:class
    {
    }
}
