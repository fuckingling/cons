﻿using Microsoft.Extensions.DependencyInjection;
using Lynn.Infastructure.Repository;
using Lynn.Infastructure.Repository.MongoDB;
using Lynn.Infastructure.Repository.BaseEntity;

namespace Lynn.Domain.DataCenter.IRepository
{
    public interface IMongoRepository<T> : IMongoBaseRepository<T> where T : Entity
    {
    }
}
