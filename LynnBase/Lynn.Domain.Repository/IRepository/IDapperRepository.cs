﻿using Microsoft.Extensions.DependencyInjection;
using Lynn.Infastructure.Repository.Dapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynn.Domain.IRepository
{
    public interface IDapperRepository: IDapperBaseRepository
    {
    }
}
