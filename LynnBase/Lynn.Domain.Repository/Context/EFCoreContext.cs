﻿using Microsoft.EntityFrameworkCore;
using Lynn.Infastructure.Repository.EFCore;

namespace Lynn.Domain.Context
{
    public class EFCoreContext : EFCoreBaseContext
    {
        public EFCoreContext(DbContextOptions options) : base(options) { }
    }
}
