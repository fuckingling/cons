﻿using Lynn.Domain.Context;
using Lynn.Domain.IRepository;
using Lynn.Infastructure.Repository.Dapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynn.Domain.Repository
{
    public class DapperRepository : DapperBaseRepository, IDapperRepository
    {
        public DapperRepository(Context.DapperContext context) : base(context)
        {
        }
    }
}
