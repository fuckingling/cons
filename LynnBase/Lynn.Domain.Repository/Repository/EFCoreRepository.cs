﻿using Lynn.Domain.Context;
using Lynn.Domain.IRepository;
using Lynn.Infastructure.Repository;
using Lynn.Infastructure.Repository.EFCore;
using Microsoft.Extensions.DependencyInjection;

namespace Lynn.Domain.Repository
{
    public class EFCoreRepository<T>: EFCoreBaseRepository<T>,IEFCoreRepository<T> where T:class
    {
        public EFCoreRepository(EFCoreContext context) : base(context)
        {
        }
    }
}
