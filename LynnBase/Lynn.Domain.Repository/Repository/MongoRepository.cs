﻿using Lynn.Domain.DataCenter.Context;
using Lynn.Domain.DataCenter.IRepository;
using Lynn.Infastructure.Repository;
using Lynn.Infastructure.Repository.BaseEntity;
using Lynn.Infastructure.Repository.MongoDB;

namespace Lynn.Domain.DataCenter.Repository
{
    public class MongoRepository<T> : MongoBaseRepository<T>, IMongoRepository<T> where T : Entity
    {
        public MongoRepository(MongoContext context) : base(context)
        {
        }
    }
}
