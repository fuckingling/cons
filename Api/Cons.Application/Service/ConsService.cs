﻿using Cons.Application.IService;
using Cons.Domain.Entities.Cons;
using Cons.Domain.Entities.Enums;
using Lynn.Domain.IRepository;
using Lynn.Infastructure.Aop.Model;
using Lynn.Infastructure.Extension;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Application.Service
{
    public class ConsService : IConsService
    {
        IEFCoreRepository<Constellation> _consRepository;
        IEFCoreRepository<ConsFortune> _fortuneRepository;
        IEFCoreRepository<FortuneRecord> _recordRepository;

        public ConsService(IEFCoreRepository<Constellation> consRepository, IEFCoreRepository<ConsFortune> fortuneRepository, IEFCoreRepository<FortuneRecord> recordRepository)
        {
            _consRepository = consRepository;
            _fortuneRepository = fortuneRepository;
            _recordRepository = recordRepository;
        }



        async Task<ConsFortune> IConsService.getConsFortuneByData(DateTime dateTime, E_Cons e_Cons)
        {
            //日期转换
            var tm = dateTime.ToString("yyyy-MM-dd").ToDateTime();
            return await _fortuneRepository.Query(x => x.Date == tm && x.E_Cons == e_Cons).FirstOrDefaultAsync();
        }
        async Task<List<Constellation>> IConsService.GetConstellations()
        {
            return await _consRepository.Query(x => true).ToListAsync();
        }

        async Task<FortuneRecord> IConsService.GetFortuneRecord(DateTime dateTime, E_Cons e_Cons, E_FortuneRecordType type)
        {
            var date = type switch
            {
                E_FortuneRecordType.周 => dateTime.AddDays(-(int)dateTime.DayOfWeek).ToString("yyyyMMdd"),
                E_FortuneRecordType.月 => dateTime.AddDays(-(int)dateTime.DayOfWeek).ToString("yyyyMM"),
                E_FortuneRecordType.年 => dateTime.AddDays(-(int)dateTime.DayOfWeek).ToString("yyyy"),
                _ => throw new Error("参数错误")
            };
            var code = $"{date}{Convert.ToDouble(e_Cons).ToString("00")}".ToInt();
            return await _recordRepository.Query(x => x.Code == code).FirstOrDefaultAsync();
        }
    }
}
