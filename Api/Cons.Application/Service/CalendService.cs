﻿using Cons.Application.IService;
using Cons.Domain.Entities.Cale;
using Lynn.Domain.IRepository;
using Lynn.Infastructure.Extension;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Application.Service
{
    public class CalendService : ICalendService
    {
        IEFCoreRepository<Calendar> _calRepository;

        public CalendService(IEFCoreRepository<Calendar> calRepository)
        {
            _calRepository = calRepository;
        }

        async Task<List<Calendar>> ICalendService.GetCalendar(int year)
        {
            var tm = DateTime.Now.ToBeiginTimeOfMonth();
            var end = tm.AddYears(year);
            return await _calRepository.Query(x => x.Date >= tm && x.Date <= end).ToListAsync();
        }

        async Task<List<Calendar>> ICalendService.GetCalendarByDays(int days)
        {
            var tm = DateTime.Now.ToBeiginTimeOfDay();
            var end = tm.AddDays(days);
            return await _calRepository.Query(x => x.Date >= tm && x.Date <= end).ToListAsync();
        }
    }
}
