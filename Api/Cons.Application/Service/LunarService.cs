﻿using Cons.Application.IService;
using Cons.Domain.Entities.Cale;
using Lynn.Domain.IRepository;
using Lynn.Infastructure.Extension;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Application.Service
{
    public class LunarService : ILunarService
    {
        IEFCoreRepository<LunarZodiac> _repository;

        public LunarService(IEFCoreRepository<LunarZodiac> repository)
        {
            _repository = repository;
        }

        async Task<LunarZodiac> ILunarService.GetLunarZodiac(DateTime dateTime)
        {
            var code = dateTime.ToBeiginTimeOfDay().ToString("yyyyMMdd").ToInt();
            return await _repository.Query(x => x.Code == code, x => x.ZodiacThings, x => x.Calendar).FirstOrDefaultAsync();
        }

        async Task<List<LunarZodiac>> ILunarService.GetLunarZodiac(DateTime start, DateTime end)
        {
            var codeStart = start.ToBeiginTimeOfDay().ToString("yyyyMMdd").ToInt();
            var codeEnd = end.ToBeiginTimeOfDay().ToString("yyyyMMdd").ToInt();
            return await _repository.Query(x => x.Code >= codeStart && x.Code <= codeEnd, x => x.ZodiacThings, x => x.Calendar).ToListAsync();
        }
    }
}
