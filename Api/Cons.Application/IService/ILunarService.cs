﻿using Cons.Domain.Entities.Cale;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Application.IService
{
    [AppService(AppServiceTargets.Scoped)]
    public interface ILunarService
    {
        /// <summary>
        /// 根据日期获取农历
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        Task<LunarZodiac> GetLunarZodiac(DateTime dateTime);
        /// <summary>
        /// 根据日期获取农历
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        Task<List<LunarZodiac>> GetLunarZodiac(DateTime start,DateTime end);
    }
}
