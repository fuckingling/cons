﻿using Cons.Domain.Entities.Cale;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Application.IService
{
    [AppService(AppServiceTargets.Scoped)]
    public interface ICalendService
    {
        /// <summary>
        /// 获取从现在开始几年的日历数据
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        Task<List<Calendar>> GetCalendar(int year);
        /// <summary>
        /// 获取从现在开始几年的日历数据
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        Task<List<Calendar>> GetCalendarByDays(int days);
    }
}
