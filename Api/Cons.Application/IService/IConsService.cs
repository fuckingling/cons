﻿using Cons.Domain.Entities.Cons;
using Cons.Domain.Entities.Enums;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Application.IService
{
    [AppService(AppServiceTargets.Scoped)]
    public interface IConsService
    {
        /// <summary>
        /// 获取星座基本信息
        /// </summary>
        /// <returns></returns>
        Task<List<Constellation>> GetConstellations();
        /// <summary>
        /// 获取指定日期指定星座的运势
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="e_Cons"></param>
        /// <returns></returns>
        Task<ConsFortune> getConsFortuneByData(DateTime dateTime, E_Cons e_Cons);
        /// <summary>
        /// 获取指定日期的周、月、年运势
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="e_Cons"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        Task<FortuneRecord> GetFortuneRecord(DateTime dateTime, E_Cons e_Cons, E_FortuneRecordType type);

    }
}
