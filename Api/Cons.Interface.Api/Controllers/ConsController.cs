﻿using Cons.Application.IService;
using Cons.Domain.Entities.Cons;
using Cons.Domain.Entities.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cons.Interface.Api.Controllers
{
    /// <summary>
    /// 星座
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ConsController : ControllerBase
    {
        IConsService _consService;

        public ConsController(IConsService consService)
        {
            _consService = consService;
        }

        /// <summary>
        /// 获取星座基本信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<Constellation>> GetConstellations() => await _consService.GetConstellations();
        /// <summary>
        /// 获取今日运势
        /// </summary>
        /// <param name="e_Cons"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ConsFortune> getTodayConsFortune(E_Cons e_Cons)
        {
            var dateTime = DateTime.Now;
            return await _consService.getConsFortuneByData(dateTime, e_Cons);
        }
        /// <summary>
        /// 获取明日运势
        /// </summary>
        /// <param name="e_Cons"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ConsFortune> getTomorrowConsFortune(E_Cons e_Cons)
        {
            var dateTime = DateTime.Now;
            var res = await _consService.getConsFortuneByData(dateTime.AddDays(1), e_Cons);
            if (res == null)
            {
                res = await _consService.getConsFortuneByData(dateTime, e_Cons);
            }
            return res;
        }
        /// <summary>
        /// 获取周、月、年运势
        /// </summary>
        /// <param name="e_Cons"></param>
        /// <param name="type">周0,月1,年2</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<FortuneRecord> GetFortuneRecord(E_Cons e_Cons, E_FortuneRecordType type)
        {

            var date = DateTime.Now;
            return await _consService.GetFortuneRecord(date, e_Cons, type);
        }
    }
}
