﻿using Cons.Application.IService;
using Cons.Domain.Entities.Cale;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Cons.Interface.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LunarController : ControllerBase
    {
        ILunarService _lunarService;

        public LunarController(ILunarService lunarService)
        {
            this._lunarService = lunarService;
        }
        /// <summary>
        /// 根据日期获取农历信息
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<LunarZodiac> GetLunarZodiac(DateTime dateTime) =>await _lunarService.GetLunarZodiac(dateTime);
        /// <summary>
        /// 根据日期获取农历信息 [start,end]
        /// </summary>
        /// <param name="start">开始时间 </param>
        /// <param name="end">结束时间</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<LunarZodiac>> GetLunarZodiacList(DateTime start,DateTime end) => await _lunarService.GetLunarZodiac(start,end);
    }
}
