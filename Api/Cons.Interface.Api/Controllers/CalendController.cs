﻿using Cons.Application.IService;
using Cons.Domain.Entities.Cale;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cons.Interface.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CalendController : ControllerBase
    {
        ICalendService _calendService;

        public CalendController(ICalendService calendService)
        {
            _calendService = calendService;
        }
        /// <summary>
        /// 获取日历数据
        /// </summary>
        /// <param name="year">多少年</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<Calendar>> GetCalendar(int year) => await _calendService.GetCalendar(year);
        /// <summary>
        /// 获取日历数据
        /// </summary>
        /// <param name="days">多少天</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<Calendar>> GetCalendarByDays(int days) => await _calendService.GetCalendarByDays(days);
    }
}
