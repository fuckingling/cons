﻿using Lynn.Infastructure.Repository.BaseEntity;
using System.ComponentModel;

namespace Cons.Domain.Entities.Base
{
    public class BaseEntity : Entity, IRowVersion, IDel
    {
        [DefaultValue(0)]
        public int RowVersion { get; set; } = 0;
        public bool Del { get; set; }
    }
}
