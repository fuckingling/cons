﻿using Lynn.Infastructure.Repository.EFCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Domain.Entities.Base
{
    public class BaseCode
    {
        /// <summary>
        /// 编号 规则：yyyyMMdd
        /// </summary>
        [Key]
        [Length("编号 规则：yyyyMMdd", 8)]
        public int Code { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Length("备注", 200)]
        public string NT { get; set; }
    }
}
