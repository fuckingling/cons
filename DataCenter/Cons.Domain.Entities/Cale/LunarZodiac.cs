﻿using Cons.Domain.Entities.Base;
using Lynn.Infastructure.Repository.EFCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Domain.Entities.Cale
{
    /// <summary>
    /// 农历黄道信息
    /// </summary>
    [Table(nameof(LunarZodiac))]
    [Index(nameof(CalenCode), IsUnique = true)]
    public class LunarZodiac: BaseCode
    {
        /// <summary>
        /// 公历编号 规则：yyyyMMdd
        /// </summary>
        [Length("公历编号 规则：yyyyMMdd", 8)]
        public int CalenCode { get; set; }
        /// <summary>
        /// 农历 月
        /// </summary>
        [Length("农历月", 10)]
        public string LunarMonth { get; set; }
        /// <summary>
        /// 农历 日
        /// </summary>
        [Length("农历日", 10)]
        public string LunarDay { get; set; }
        /// <summary>
        /// 彭祖百忌
        /// </summary>
        [Length("彭祖百忌", 50)]
        public string PZBJ { get; set; }
        /// <summary>
        /// 胎神占方
        /// </summary>
        [Length("胎神占方", 50)]
        public string TSZF { get; set; }
        /// <summary>
        /// 年五行
        /// </summary>
        [Length("年五行", 50)]
        public string NWX { get; set; }
        /// <summary>
        /// 月五行
        /// </summary>
        [Length("月五行", 50)]
        public string YWX { get; set; }
        /// <summary>
        /// 日五行
        /// </summary>
        [Length("日五行", 50)]
        public string RWX { get; set; }
        /// <summary>
        /// 儒略日
        /// </summary>
        [Length("儒略日", 50)]
        public string RLR { get; set; }
        /// <summary>
        /// 冲
        /// </summary>
        [Length("冲", 50)]
        public string CHONG { get; set; }
        /// <summary>
        /// 六曜
        /// </summary>
        [Length("六曜", 50)]
        public string LY { get; set; }
        /// <summary>
        /// 季节
        /// </summary>
        [Length("季节", 50)]
        public string JJ { get; set; }
        /// <summary>
        /// 星宿
        /// </summary>
        [Length("星宿", 50)]
        public string XX { get; set; }
        /// <summary>
        /// 节气
        /// </summary>
        [Length("节气", 50)]
        public string JQ { get; set; }
        /// <summary>
        /// 伊斯兰历
        /// </summary>
        [Length("伊斯兰历", 50)]
        public string YSLL { get; set; }
        /// <summary>
        /// 煞
        /// </summary>
        [Length("煞", 50)]
        public string SHA { get; set; }
        /// <summary>
        /// 十二神
        /// </summary>
        [Length("十二神", 50)]
        public string SES { get; set; }
        /// <summary>
        /// 宜
        /// </summary>
        [Length("宜", 250)]
        public string YI { get; set; }
        /// <summary>
        /// 忌
        /// </summary>
        [Length("忌", 250)]
        public string JI { get; set; }
        [ForeignKey("LunarCode")]
        public virtual List<ZodiacThings> ZodiacThings { get; set; }
        [ForeignKey(nameof(CalenCode))]
        public virtual Calendar Calendar { get; set; }
    }
}
