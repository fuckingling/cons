﻿using Cons.Domain.Entities.Base;
using Cons.Domain.Entities.Cons;
using Cons.Domain.Entities.Enums;
using Lynn.Infastructure.Repository.EFCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Domain.Entities.Cale
{
    /// <summary>
    /// 日历表
    /// </summary>
    [Table(nameof(Calendar))]
    [Index(nameof(Date),IsUnique =true)]
    [Index(nameof(Year), nameof(Month), nameof(Day), IsUnique = true)]
    public class Calendar: BaseCode
    {
        /// <summary>
        /// 公立日期
        /// </summary>
        [Length("日期")]
        public DateTime Date { get; set; }
        /// <summary>
        /// 公立日期 年
        /// </summary>
        [Length("年", 4)]
        public int Year { get; set; }
        /// <summary>
        /// 公立日期
        /// </summary>
        [Length("月", 2)]
        public int Month { get; set; }
        /// <summary>
        /// 公立日期
        /// </summary>
        [Length("日", 2)]
        public int Day { get; set; }
        /// <summary>
        /// 农历 年
        /// </summary>
        [Length("农历年", 10)]
        public string LunarYear { get; set; }
        /// <summary>
        /// 生肖年
        /// </summary>
        [Length("生肖年", 10)]
        public E_ChZodiac ChZodiac { get; set; }
        /// <summary>
        /// 农历 月
        /// </summary>
        [Length("农历月", 10)]
        public string LunarMonth { get; set; }
        /// <summary>
        /// 农历 日
        /// </summary>
        [Length("农历日", 10)]
        public string LunarDay { get; set; }
        /// <summary>
        /// 星期
        /// </summary>
        [Length("星期", 1)]
        public E_Week Week { get; set; }
        /// <summary>
        /// 节日
        /// </summary>
        [Length("节日", 50)]
        public string Festival { get; set; }
        /// <summary>
        /// 星座
        /// </summary>
        [Length("星座", 2)]
        public E_Cons Cons { get; set; }
        [ForeignKey(nameof(Cons))]
        public virtual Constellation Constellation { get; set; }
        [InverseProperty("Calendar")]
        public virtual LunarZodiac LunarZodiac { get; set; }
    }
}
