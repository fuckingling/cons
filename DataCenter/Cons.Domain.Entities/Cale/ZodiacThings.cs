﻿using Cons.Domain.Entities.Base;
using Lynn.Infastructure.Repository.EFCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Domain.Entities.Cale
{
    [Index(nameof(LunarCode), IsUnique = false)]
    [Table(nameof(ZodiacThings))]
    public class ZodiacThings: BaseEntity
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Length("名称", 20)]
        public string ThingsName { get; set; }
        /// <summary>
        /// 提示
        /// </summary>
        [Length("提示", 200)]
        public string ThingsTips{ get; set; }
        /// <summary>
        /// 是否可以做
        /// </summary>
        [Length("是否可以做 对应宜")]
        public bool CanDo { get; set; }
        /// <summary>
        /// 农历Code
        /// </summary>
        [Length("农历Code", 20)]
        public int LunarCode { get; set; }
    }
}
