﻿using Cons.Domain.Entities.Base;
using Cons.Domain.Entities.Enums;
using Lynn.Infastructure.Repository.EFCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Domain.Entities.Cons
{
    [Index(nameof(E_Cons), nameof(Date), IsUnique = true)]
    [Table(nameof(ConsFortune))]
    public class ConsFortune
    {
        /// <summary>
        /// 运势编号 日历编号+星座编号
        /// </summary>
        [Key]
        [Length("运势编号 日历编号+星座编号", 10)]
        public int Code { get; set; }
        [Length("星座编号", 2)]
        public E_Cons E_Cons { get; set; }
        /// <summary>
        /// 日历编号
        /// </summary>
        [Length("日历编号", 8)]
        public int CalCode { get; set; }
        [Length("时间")]
        public DateTime Date { get; set; }
        /// <summary>
        /// 速配星座
        /// </summary>
        [Length("速配星座", 20)]
        public string Friend { get; set; }
        /// <summary>
        /// 幸运色
        /// </summary>
        [Length("幸运色", 20)]
        public string Color { get; set; }
        /// <summary>
        /// 健康
        /// </summary>
        [Length("健康", 6)]
        public int Health { get; set; }
        /// <summary>
        /// 爱情
        /// </summary>
        [Length("爱情", 6)]
        public int Love { get; set; }
        /// <summary>
        /// 工作
        /// </summary>
        [Length("工作", 6)]
        public int Work { get; set; }
        /// <summary>
        /// 财富
        /// </summary>
        [Length("财富", 6)]
        public int Money { get; set; }
        /// <summary>
        /// 幸运数字
        /// </summary>
        [Length("幸运数字", 6)]
        public int Number { get; set; }
        /// <summary>
        /// 总览
        /// </summary>
        [Length("总览", 500)]
        public string Summary { get; set; }
        /// <summary>
        /// 综合分数
        /// </summary>
        [Length("综合分数", 6)]
        public int all { get; set; }
        /// <summary>
        /// 周运势编码
        /// </summary>
        [Length("周运势编码", 10)]
        public int WeekCode { get; set; }
        /// <summary>
        /// 月运势编码
        /// </summary>
        [Length("月运势编码", 10)]
        public int MonthCode { get; set; }
        /// <summary>
        /// 年运势编码
        /// </summary>
        [Length("年运势编码", 10)]
        public int YearCode { get; set; }
        /// <summary>
        /// 周运势
        /// </summary>
        [ForeignKey(nameof(WeekCode))]
        public virtual FortuneRecord WeekFortune { get; set; }
        /// <summary>
        /// 月运势
        /// </summary>
        [ForeignKey(nameof(MonthCode))]
        public virtual FortuneRecord MonthkFortune { get; set; }
        /// <summary>
        /// 年运势
        /// </summary>
        [ForeignKey(nameof(YearCode))]
        public virtual FortuneRecord YearFortune { get; set; }
    }
}
