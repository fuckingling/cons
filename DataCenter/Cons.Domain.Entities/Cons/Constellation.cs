﻿using Cons.Domain.Entities.Base;
using Cons.Domain.Entities.Enums;
using Lynn.Infastructure.Repository.EFCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cons.Domain.Entities.Cons
{
    /// <summary>
    /// 星座
    /// </summary>
    [Table(nameof(Constellation))]
    public class Constellation
    {
        /// <summary>
        /// 星座编号
        /// </summary>
        [Key, Length("星座编号", 2)]
        public E_Cons Code { get; set; }
        /// <summary>
        /// 星座名称
        /// </summary>
        [Length("星座名称", 10)]
        public string Name { get; set; }
        /// <summary>
        /// 开始日期 MDD格式 3月1日=301
        /// </summary>
        [Length("开始日期 MDD格式 3月1日=301 包含", 4)]
        public int StartDate { get; set; }
        /// <summary>
        /// 结束日期 MDD格式 3月1日=301
        /// </summary>
        [Length("结束日期 MDD格式 3月1日=301 包含", 4)]
        public int EndDate { get; set; }
        /// <summary>
        /// 星座特点
        /// </summary>
        [Length("星座特点", 20)]
        public string XZTD { get; set; }
        /// <summary>
        /// 四象属性
        /// </summary>
        [Length("四象属性", 20)]
        public string SXSX { get; set; }
        /// <summary>
        /// 掌管宫位
        /// </summary>
        [Length("掌管宫位", 20)]
        public string ZGGW { get; set; }
        /// <summary>
        /// 阴阳属性
        /// </summary>
        [Length("阴阳属性", 20)]
        public string YYSX { get; set; }
        /// <summary>
        /// 最大特征
        /// </summary>
        [Length("最大特征", 20)]
        public string ZDTZ { get; set; }
        /// <summary>
        /// 主管行星
        /// </summary>
        [Length("主管行星", 20)]
        public string ZGXX { get; set; }
        /// <summary>
        /// 幸运颜色
        /// </summary>
        [Length("幸运颜色", 20)]
        public string XYYS { get; set; }
        /// <summary>
        /// 吉祥饰物
        /// </summary>
        [Length("吉祥饰物", 20)]
        public string JXSW { get; set; }
        /// <summary>
        /// 幸运号码
        /// </summary>
        [Length("幸运号码", 20)]
        public int XYHM { get; set; }
        /// <summary>
        /// 开运金属
        /// </summary>
        [Length("开运金属", 20)]
        public string KYJS { get; set; }

        /// <summary>
        /// 表现
        /// </summary>
        [Length("表现", 50)]
        public string Performance { get; set; }
        /// <summary>
        /// 优点
        /// </summary>
        [Length("优点", 50)]
        public string Merit { get; set; }
        /// <summary>
        /// 缺点
        /// </summary>
        [Length("缺点", 50)]
        public string Defect { get; set; }

        /// <summary>
        /// 基本特质
        /// </summary>
        [Length("基本特质", 250)]
        public string JBTZ { get; set; }
        /// <summary>
        /// 具体特质
        /// </summary>
        [Length("具体特质", 250)]
        public string JTTZ { get; set; }
        /// <summary>
        /// 行事风格
        /// </summary>
        [Length("行事风格", 250)]
        public string XSFG { get; set; }
        /// <summary>
        /// 个性盲点
        /// </summary>
        [Length("个性盲点", 250)]
        public string GXMD { get; set; }
        /// <summary>
        /// 总结
        /// </summary>
        [Length("总结", 250)]
        public string Summary { get; set; }
    }
}
