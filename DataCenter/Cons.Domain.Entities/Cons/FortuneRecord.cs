﻿using Cons.Domain.Entities.Base;
using Cons.Domain.Entities.Enums;
using Lynn.Infastructure.Repository.EFCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Domain.Entities.Cons
{
    [Index(nameof(E_Cons), nameof(Type))]
    [Table(nameof(FortuneRecord))]
    public class FortuneRecord
    {
        /// <summary>
        /// 运势编号 日历编号+星座编号 年则是yyyy 月则是yyyyMM 周则是yyyyMMdd
        /// </summary>
        [Key]
        [Length("运势编号 日历编号+星座编号", 10)]
        public int Code { get; set; }
        [Length("日期 每个周期的第一天", 2)]
        public DateTime Date { get; set; }
        /// <summary>
        /// 健康
        /// </summary>
        [Length("健康", 500)]
        public string Health { get; set; }
        /// <summary>
        /// 爱情
        /// </summary>
        [Length("爱情", 500)]
        public string Love { get; set; }
        /// <summary>
        /// 工作
        /// </summary>
        [Length("工作", 500)]
        public string Work { get; set; }
        /// <summary>
        /// 财富
        /// </summary>
        [Length("财富", 500)]
        public string Money { get; set; }
        /// <summary>
        /// 总览 月和年才有
        /// </summary>
        [Length("总览 月和年才有", 500)]
        public string Summary { get; set; }
        /// <summary>
        /// 幸运石头-年才有
        /// </summary>
        [Length("幸运石头-年才有", 20)]
        public string LuckeyStone { get; set; }
        [Length("星座编号", 2)]
        public E_Cons E_Cons { get; set; }
        /// <summary>
        /// 运势类型 周、月、年枚举
        /// </summary>
        [Length("运势类型 周、月、年枚举", 2)]
        public E_FortuneRecordType Type { get; set; }
    }
}
