﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Domain.Entities.Enums
{
    /// <summary>
    /// 星座
    /// </summary>
    public enum E_Cons
    {
        白羊座 = 1,
        金牛座,
        双子座,
        巨蟹座,
        狮子座,
        处女座,
        天秤座,
        天蝎座,
        射手座,
        摩羯座,
        水瓶座,
        双鱼座
    }
}
