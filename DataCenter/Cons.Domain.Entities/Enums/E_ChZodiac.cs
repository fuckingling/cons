﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Domain.Entities.Enums
{
    /// <summary>
    /// 生肖
    /// </summary>
    public enum E_ChZodiac
    {
        //鼠、牛、虎、兔、龙、蛇、马、羊、猴、鸡、狗、猪
        鼠 = 1,
        牛,
        虎,
        兔,
        龙,
        蛇,
        马,
        羊,
        猴,
        鸡,
        狗,
        猪,
    }
}
