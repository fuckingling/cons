﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Domain.Entities.Enums
{
    /// <summary>
    /// 周
    /// </summary>
    public enum E_Week
    {
        星期日 = 0,
        星期一 = 1,
        星期二,
        星期三,
        星期四,
        星期五,
        星期六,
    }
}
