﻿using Lynn.Infastructure.AutoTask;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Service.JuheData
{
    public class JuheTask : TaskBackgroundService
    {
        protected override async Task DoWork(IServiceProvider provider)
        {
            var service = provider.GetRequiredService<JuheService>();
            await service.InsertData();

        }
    }
}
