﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Service.JuheData.Base
{
    public class YearInfo
    {
        public string name { get; set; }
        public string date { get; set; }
        public int year { get; set; }
        /// <summary>
        /// 年度密码
        /// </summary>
        public Mima mima { get; set; }
        /// <summary>
        /// 事业运
        /// </summary>
        public string[] career { get; set; }
        /// <summary>
        /// 感情运
        /// </summary>
        public string[] love { get; set; }
        /// <summary>
        /// 健康
        /// </summary>
        public string[] health { get; set; }
        /// <summary>
        /// 财运
        /// </summary>
        public string[] finance { get; set; }
        /// <summary>
        /// 幸运石头
        /// </summary>
        public string luckeyStone { get; set; }
        public string future { get; set; }
        public int resultcode { get; set; }
        public int error_code { get; set; }
    }

    public class Mima
    {
        /// <summary>
        /// 概述
        /// </summary>
        public string info { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string[] text { get; set; }
    }

}
