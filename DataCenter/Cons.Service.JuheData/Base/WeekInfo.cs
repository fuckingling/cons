﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Service.JuheData.Base
{
    public class WeekInfo
    {
        public string name { get; set; }
        public int weekth { get; set; }
        public string date { get; set; }
        public string health { get; set; }
        public string job { get; set; }
        public string love { get; set; }
        public string money { get; set; }
        public string work { get; set; }
        public int resultcode { get; set; }
        public int error_code { get; set; }
    }

}
