﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Service.JuheData.Base
{
    public class MonthInfo
    {
        public string date { get; set; }
        public string name { get; set; }
        public int month { get; set; }
        public string health { get; set; }
        public string all { get; set; }
        public string love { get; set; }
        public string money { get; set; }
        public string work { get; set; }
        public string happyMagic { get; set; }
        public int resultcode { get; set; }
        public int error_code { get; set; }
    }

}
