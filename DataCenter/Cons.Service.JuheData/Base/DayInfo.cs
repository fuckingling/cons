﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cons.Service.JuheData.Base
{
    public class DayInfo
    {
        public int date { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 速配星座
        /// </summary>
        public string friend { get; set; }
        /// <summary>
        /// 幸运色
        /// </summary>
        public string color { get; set; }
        /// <summary>
        /// 时间
        /// </summary>
        public string datetime { get; set; }
        /// <summary>
        /// 健康
        /// </summary>
        public int health { get; set; }
        /// <summary>
        /// 爱情
        /// </summary>
        public int love { get; set; }
        /// <summary>
        /// 工作
        /// </summary>
        public int work { get; set; }
        /// <summary>
        /// 财富
        /// </summary>
        public int money { get; set; }
        /// <summary>
        /// 幸运数字
        /// </summary>
        public int number { get; set; }
        /// <summary>
        /// 总览
        /// </summary>
        public string summary { get; set; }
        /// <summary>
        /// 综合分时
        /// </summary>
        public int all { get; set; }
        public int resultcode { get; set; }
        public int error_code { get; set; }
    }

}
