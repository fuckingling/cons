using Lynn.Domain.Context;
using Lynn.Infastructure.Aop;
using Lynn.Infastructure.Net;
using Lynn.Infastructure.Repository;
using Lynn.Infastructure.Utils;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Cons.Interface.Spider
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers().AddJsonOptions(option=> {


                option.JsonSerializerOptions.NumberHandling = JsonHelper.options.NumberHandling;
                option.JsonSerializerOptions.DefaultIgnoreCondition = JsonHelper.options.DefaultIgnoreCondition;
                foreach (var item in JsonHelper.options.Converters) {
                    option.JsonSerializerOptions.Converters.Add(item);
                }
                option.JsonSerializerOptions.Encoder = JsonHelper.options.Encoder;
                option.JsonSerializerOptions.PropertyNamingPolicy = JsonHelper.options.PropertyNamingPolicy;
                option.JsonSerializerOptions.WriteIndented = JsonHelper.options.WriteIndented;
                option.JsonSerializerOptions.MaxDepth = JsonHelper.options.MaxDepth;
            });
            //数据库
            services.AddEFCore<EFCoreContext>(Configuration.GetConnectionString("database"), showSql: true).Migrate<EFCoreContext>();
            services.AddHttpService(opt=> {
                opt.ReTry = 2;
            });
            //根据特性进行自动注入
            services.AddAppServiceByAttribute();
            //接口文档
            services.AddApiDoc(new OpenApiInfo { Title = "接口文档", Version = "1", Description = "接口详细描述" },
                $"{Assembly.GetExecutingAssembly().GetName().Name}.xml");
            services.AddAutoTasks(Configuration.GetSection("Tasks"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //文档
                app.UseApiDoc();
            }

            app.UseHttpsRedirection();

            //结果重构中间件
            app.UseResultBind();

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors(p => {
                p.AllowAnyOrigin();
                p.AllowAnyHeader();
                p.AllowAnyMethod();
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
