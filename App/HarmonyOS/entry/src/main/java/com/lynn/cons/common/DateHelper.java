package com.lynn.cons.common;

import ohos.global.icu.text.SimpleDateFormat;
import ohos.global.icu.util.Calendar;

import java.text.ParseException;
import java.util.Date;

public class DateHelper {
    /**
     * 获取今天
     *
     * @return String
     */
    public static String getToday(String format) {
        return new SimpleDateFormat(format).format(Calendar.getInstance().getTime());
    }

    public static Date getDate(String date, String format) {
        try {
            return new SimpleDateFormat(format).parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取昨天
     *
     * @return String
     */
    public static String getYestoday(String format) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Date time = cal.getTime();
        return new SimpleDateFormat(format).format(time);
    }

    /**
     * 获取几天后的日期
     * @param date
     * @param days
     * @return
     */
    public static Date getDateLater(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 5);
        return cal.getTime();
    }

    /**
     * 获取本月开始日期
     *
     * @return String
     **/
    public static String getMonthStart(String format) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        Date time = cal.getTime();
        return new SimpleDateFormat(format).format(time);
    }

    /**
     * 格式化时间
     *
     * @param format
     * @param date
     * @return
     */
    public static String geFormat(Date date, String format) {
        return new SimpleDateFormat(format).format(date);
    }

    /**
     * 获取本月最后一天
     *
     * @return String
     **/
    public static String getMonthEnd(String format) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date time = cal.getTime();
        return new SimpleDateFormat(format).format(time);
    }

    /**
     * 获取本周的第一天
     *
     * @return String
     **/
    public static String getWeekStart(String format) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.WEEK_OF_MONTH, 0);
        cal.set(Calendar.DAY_OF_WEEK, 1);
        Date time = cal.getTime();
        return new SimpleDateFormat(format).format(time);
    }

    /**
     * 获取本周的最后一天
     *
     * @return String
     **/
    public static String getWeekEnd(String format) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, cal.getActualMaximum(Calendar.DAY_OF_WEEK));
        cal.add(Calendar.DAY_OF_WEEK, 1);
        Date time = cal.getTime();
        return new SimpleDateFormat(format).format(time);
    }

    /**
     * 获取本年的第一天
     *
     * @return String
     **/
    public static String getYearStart(String format) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, calendar.getActualMinimum(Calendar.MONTH));
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        Date year = calendar.getTime();
        return new SimpleDateFormat(format).format(year);
    }

    /**
     * 获取本年的最后一天
     *
     * @return String
     **/
    public static String getYearEnd(String format) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, calendar.getActualMaximum(Calendar.MONTH));
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date currYearLast = calendar.getTime();
        return new SimpleDateFormat(format).format(currYearLast);
    }
}
