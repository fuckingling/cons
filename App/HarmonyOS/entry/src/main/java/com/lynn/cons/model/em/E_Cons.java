package com.lynn.cons.model.em;

import java.util.Arrays;

public enum E_Cons {
    白羊座(1, "3.21", "4.19", "byz"),
    金牛座(2, "4.20", "5.20", "jnz"),
    双子座(3, "5.21", "6.21", "szz"),
    巨蟹座(4, "6.22", "7.22", "jxz"),
    狮子座(5, "7.23", "8.22", "sz"),
    处女座(6, "8.23", "9.22", "cnz"),
    天秤座(7, "9.23", "10.23", "tpz"),
    天蝎座(8, "10.24", "11.22", "txz"),
    射手座(9, "11.23", "12.21", "ssz"),
    摩羯座(10, "12.22", "1.19", "mjz"),
    水瓶座(11, "1.20", "2.18", "spz"),
    双鱼座(12, "2.19", "3.20", "syz");

    public int getIndex() {
        return Index;
    }

    public void setIndex(int index) {
        Index = index;
    }

    private int Index;
    private String Start;
    private String End;
    private String image;

    public String getStart() {
        return Start;
    }

    public void setStart(String start) {
        Start = start;
    }

    public String getEnd() {
        return End;
    }

    public void setEnd(String end) {
        End = end;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private E_Cons(int index, String start, String end, String image) {
        this.Index = index;
        this.Start = start;
        this.End = end;
        this.image = image;
    }

    public static String get(int index) {
        return Arrays.stream(E_Cons.values()).filter(x -> x.Index == index).findFirst().get().name();
    }
}
