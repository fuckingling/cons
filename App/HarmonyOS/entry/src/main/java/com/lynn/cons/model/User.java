package com.lynn.cons.model;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.PrimaryKey;

@Entity(tableName = "user", ignoredColumns = "ignoreColumn")
public class User extends OrmObject {
    @PrimaryKey(autoGenerate = false)
    private Integer Id;
    /**
     * 星座编号
     */
    private Integer ConCode;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Integer getConCode() {
        return ConCode;
    }

    public void setConCode(Integer conCode) {
        ConCode = conCode;
    }

    public String getConName() {
        return ConName;
    }

    public void setConName(String conName) {
        ConName = conName;
    }

    /**
     * 星座名称
     */
    private String ConName;
}
