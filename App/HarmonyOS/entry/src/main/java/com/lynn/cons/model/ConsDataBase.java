package com.lynn.cons.model;

import ohos.data.orm.OrmDatabase;
import ohos.data.orm.annotation.Database;

@Database(entities = {User.class,Calend.class, TaskInfo.class,ConsFortune.class}, version = 1)
public abstract class ConsDataBase extends OrmDatabase {
}
