package com.lynn.cons.model;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.Index;
import ohos.data.orm.annotation.PrimaryKey;

import java.util.Date;
@Entity(tableName = "ConsFortune", ignoredColumns = "ignoreColumn",
        indices = {@Index(value = {"E_Cons","CalCode"}, name = "con_cal_index")})
public class ConsFortune extends OrmObject {
    @PrimaryKey(autoGenerate = false)
    ///<summary>
    /// 运势编号 日历编号+星座编号
    /// </summary>
    public int Code;
    /**
     * 星座编号
     */
    public int E_Cons;

    public int getE_Cons() {
        return E_Cons;
    }

    public void setE_Cons(int e_Cons) {
        E_Cons = e_Cons;
    }

    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public int getCalCode() {
        return CalCode;
    }

    public void setCalCode(int calCode) {
        CalCode = calCode;
    }

    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date date) {
        Date = date;
    }

    public String getFriend() {
        return Friend;
    }

    public void setFriend(String friend) {
        Friend = friend;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public int getHealth() {
        return Health;
    }

    public void setHealth(int health) {
        Health = health;
    }

    public int getLove() {
        return Love;
    }

    public void setLove(int love) {
        Love = love;
    }

    public int getWork() {
        return Work;
    }

    public void setWork(int work) {
        Work = work;
    }

    public int getMoney() {
        return Money;
    }

    public void setMoney(int money) {
        Money = money;
    }

    public int getNumber() {
        return Number;
    }

    public void setNumber(int number) {
        Number = number;
    }

    public String getSummary() {
        return Summary;
    }

    public void setSummary(String summary) {
        Summary = summary;
    }

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }

    /// <summary>
    /// 日历编号
    /// </summary>
    public int CalCode;
    public Date Date;
    /// <summary>
    /// 速配星座
    /// </summary>
    public String Friend;
    /// <summary>
    /// 幸运色
    /// </summary>
    public String Color;
    /// <summary>
    /// 健康
    /// </summary>
    public int Health;
    /// <summary>
    /// 爱情
    /// </summary>
    public int Love;
    /// <summary>
    /// 工作
    /// </summary>
    public int Work;
    /// <summary>
    /// 财富
    /// </summary>
    public int Money;
    /// <summary>
    /// 幸运数字
    /// </summary>
    public int Number;
    /// <summary>
    /// 总览
    /// </summary>
    public String Summary;
    /// <summary>
    /// 综合分数
    /// </summary>
    public int all;
}
