package com.lynn.cons.common.net;

import ohos.hiviewdfx.HiLogLabel;
import ohos.net.NetHandle;
import ohos.net.NetManager;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class Request {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD000F00, "测试");
    private NetManager netManager;

    private HttpURLConnection getConn(String url, String method) throws IOException {
        netManager = NetManager.getInstance(null);
        URL httpUrl = new URL(url);
        NetHandle netHandle = netManager.getDefaultNet();
        if (netHandle == null) {
            return null;
        }
        HttpURLConnection conn = (HttpURLConnection) netHandle.openConnection(httpUrl, java.net.Proxy.NO_PROXY);
        conn.setConnectTimeout(3 * 1000);
        conn.setReadTimeout(3 * 1000);
        conn.setRequestMethod(method);
        return conn;
    }

    /**
     * 添加请求头
     *
     * @param conn
     * @param headers
     */
    private void setHeader(HttpURLConnection conn, Map<String, String> headers) {
        if (headers == null) return;
        headers.entrySet().stream().forEach(e -> conn.setRequestProperty(e.getKey(), e.getValue()));
    }

    String getData(String requestURL, Map<String, String> headers) throws IOException {
        HttpURLConnection conn = null;
        try {
            conn = getConn(requestURL, "GET");
            conn.setDoInput(true);
            if (headers != null) {
                setHeader(conn, headers);
            }
            conn.connect();
            return getString(conn.getInputStream());
        } catch (Exception e) {
            throw e;
        }
    }

    String postData(String url, String body, String bodyType, Map<String, String> headers) throws IOException {
        HttpURLConnection conn = null;
        try {
            conn = getConn(url, "POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            if (bodyType != null && bodyType != "") {
                conn.setRequestProperty("Content-Type", bodyType);
            }
            if (headers != null) {
                setHeader(conn, headers);
            }
            conn.connect();
            if (body != null && body != "") {
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), "UTF-8"));
                writer.write(body);
                writer.close();
            }
            String result = getString(conn.getInputStream());
            return result;
        } catch (Exception e) {
            throw e;
        }
    }

    private static String getString(InputStream stream) {
        String buf;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "utf-8"));
            StringBuilder sb = new StringBuilder();
            String line = "";
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            stream.close();
            buf = sb.toString();
            return buf;

        } catch (Exception e) {
            return null;
        }
    }
}
