package com.lynn.cons.model.em;

public enum E_FortuneRecordType {
    周(0),
    月(1),
    年(2);
    public int getIndex() {
        return Index;
    }

    public void setIndex(int index) {
        Index = index;
    }
    private int Index;

    private E_FortuneRecordType(int index) {
        this.Index = index;
    }
}
