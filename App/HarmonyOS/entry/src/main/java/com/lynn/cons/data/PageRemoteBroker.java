package com.lynn.cons.data;

import com.lynn.cons.common.DateHelper;
import com.lynn.cons.common.net.HttpHelper;
import com.lynn.cons.model.TaskInfo;
import com.lynn.cons.model.em.E_Cons;
import com.lynn.cons.widget.controller.FormController;
import com.lynn.cons.widget.controller.FormControllerManager;
import ohos.app.Context;
import ohos.rpc.*;
import ohos.utils.zson.ZSONArray;
import ohos.utils.zson.ZSONObject;

import java.text.DecimalFormat;
import java.util.*;

public class PageRemoteBroker extends RemoteObject implements IRemoteBroker {
    private static final int SUCCESS = HttpHelper.RESULT_SUCCESS;
    private static final int ERROR = 500;
    private Context context;
    public PageRemoteBroker(Context context) {
        super("MyService_MyRemote");
        context=this.context;
    }

    @Override
    public boolean onRemoteRequest(int code, MessageParcel data, MessageParcel reply, MessageOption option) throws RemoteException {
        String dataStr = data.readString();
        ZSONObject param = ZSONObject.stringToZSON(dataStr);
        ConsDataService service = ConsDataService.getInstance();
        EventType type = EventType.get(code);
        switch (type) {
            case GET_TASK_BY_TM: {
                Date date = DateHelper.getDate(param.get("date").toString(), "yyyy-MM-dd");
                List<TaskInfo> list = service.getTaskByTM(date);
                ReplySuccess(reply, list);
                break;
            }
            case GET_TASK_IN_WEEK: {
                List<TaskInfo> list = service.getTaskInWeek();
                ReplySuccess(reply, list);
                break;
            }
            case DELETE_TASK: {
                int id = param.getIntValue("id");
                boolean flag = service.deleteTask(id);
                ReplySuccess(reply, flag);
                break;
            }
            case FINISH_Task: {
                int id = param.getIntValue("id");
                boolean flag = service.finishTask(id);
                ReplySuccess(reply, flag);
                break;
            }
            case ADD_TASK: {
                String title = param.get("title").toString();
                String content = param.get("content").toString();
                Date date = DateHelper.getDate(param.get("date").toString(), "yyyy-MM-dd");
                TaskInfo info = null;
                try {
                    info = service.addTask(title, content, date);
                    ReplySuccess(reply, info);
                } catch (Exception exception) {
                    exception.printStackTrace();
                    ReplyError(reply, exception.getMessage());
                }
                break;
            }
            case UPDATE_USER_CON: {
                int id = param.getIntValue("id");
                boolean flag = service.updateUserCon(id);
                ReplySuccess(reply, flag);
                break;
            }
            case GET_USER_CON: {
                E_Cons con = service.getUserCon();
                ReplySuccess(reply, con);
                break;
            }
            default: {
                ReplyError(reply, "参数错误");
                return false;
            }
        }
        //更新卡片
        FormControllerManager formControllerManager = FormControllerManager.getInstance(context);
        if (formControllerManager == null) return true;
        List<Long> formList = formControllerManager.getAllFormIdFromSharePreference();
        for (long formId : formList) {
            FormController controller = formControllerManager.getController(formId);
            if (controller == null) continue;
            controller.updateFormData();
        }
        return true;
    }

    private Map<String, Object> getResult(int code, Object data, String msg) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("code", code);
        result.put("data", data);
        result.put("msg", msg);
        return result;
    }

    /**
     * 回复消息
     *
     * @param reply
     * @param data
     */
    void ReplySuccess(MessageParcel reply, Object data) {
        reply.writeString(ZSONObject.toZSONString(getResult(SUCCESS, data, "")));
    }

    /**
     * 回复消息
     *
     * @param reply
     * @param msg
     */
    void ReplyError(MessageParcel reply, String msg) {
        reply.writeString(ZSONObject.toZSONString(getResult(ERROR, msg, "")));
    }

    @Override
    public IRemoteObject asObject() {
        return this;
    }
}