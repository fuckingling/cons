package com.lynn.cons.common.net;

import com.lynn.cons.common.ThreadPoolHelper;
import ohos.utils.zson.ZSONArray;
import ohos.utils.zson.ZSONObject;

import java.util.Map;
import java.util.function.Consumer;

public class HttpHelper {
    private static final String METHOD_GET = "GET";
    private static final String METHOD_POST = "POST";

    public static final String FILE_TYPE_FILE = "file/*";
    public static final String FILE_TYPE_IMAGE = "image/*";
    public static final String FILE_TYPE_AUDIO = "audio/*";
    public static final String FILE_TYPE_VIDEO = "video/*";

    public static final int RESULT_SUCCESS = 200;

    private static class SingletonClassInstance {
        private static final Request instance = new Request();
    }

    private HttpHelper() {
    }

    public static final Request getInstance() {
        return SingletonClassInstance.instance;
    }

    public static void get(String url, Map<String, String> headers, Consumer<ZSONObject> success, Consumer<Exception> fail) {
        ThreadPoolHelper.submit(() -> {
            try {
                String json = getInstance().getData(url, headers);
//                json=json.replace(" null","''");
                ZSONObject result = ZSONObject.stringToZSON(json);
                if (result.getInteger("code") != RESULT_SUCCESS) {
                    fail.accept(new Exception(result.getString("msg")));
                    return;
                }
                ZSONObject data=result.getZSONObject("data");
                success.accept(data);
            } catch (Exception ex) {
                fail.accept(ex);
            }
        });
    }
    public static void getList(String url, Map<String, String> headers, Consumer<ZSONArray> success, Consumer<Exception> fail) {
        ThreadPoolHelper.submit(() -> {
            try {
                String json = getInstance().getData(url, headers);
//                json=json.replace(" null","''");
                ZSONObject result = ZSONObject.stringToZSON(json);
                if (result.getInteger("code") != RESULT_SUCCESS) {
                    fail.accept(new Exception(result.getString("msg")));
                    return;
                }
                ZSONArray data=result.getZSONArray("data");
                success.accept(data);
            } catch (Exception ex) {
                fail.accept(ex);
            }
        });
    }
    public static void post(String url, String body, String bodyType, Map<String, String> headers, Consumer<Object> success, Consumer<Exception> fail) {
        ThreadPoolHelper.submit(() -> {
            try {
                String json = getInstance().postData(url, body, bodyType, headers);
                Result result = ZSONObject.stringToClass(json, Result.class);
                if (result.getCode() != RESULT_SUCCESS) {
                    fail.accept(new Exception(result.getMsg()));
                    return;
                }
                success.accept(result.getData());
            } catch (Exception ex) {
                fail.accept(ex);
            }
        });
    }
}
