package com.lynn.cons.data;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.rpc.IRemoteObject;

public class DataServiceAbility extends Ability {

    private static DataServiceAbility instance;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
    }
    private PageRemoteBroker remote = new PageRemoteBroker(this);

    // FA在请求PA服务时会调用Ability.connectAbility连接PA，连接成功后，需要在onConnect返回一个remote对象，供FA向PA发送消息
    @Override
    protected IRemoteObject onConnect(Intent intent) {
        super.onConnect(intent);
        return remote.asObject();
    }
    /**
     * BatteryInternalAbility
     *
     * @return If the instance is NULL, Get new instance. Otherwise, instance is returned.
     */
    public static DataServiceAbility getInstance() {
        if (instance == null) {
            synchronized (DataServiceAbility.class) {
                if (instance == null) {
                    instance = new DataServiceAbility();
                }
            }
        }
        return instance;
    }
}