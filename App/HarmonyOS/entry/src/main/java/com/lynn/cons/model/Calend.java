package com.lynn.cons.model;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.Index;
import ohos.data.orm.annotation.PrimaryKey;

import java.util.Date;
@Entity(tableName = "calend", ignoredColumns = "ignoreColumn",
        indices = {@Index(value = {"Date"}, name = "Date_index")})
public class Calend extends OrmObject {
    @PrimaryKey(autoGenerate = false)
    private int Code;
    /// <summary>
    /// 公立日期
    /// </summary>
    private Date Date;
    /// <summary>
    /// 公立日期 年
    /// </summary>
    private int Year;
    /// <summary>
    /// 公立日期
    /// </summary>
    private int Month;
    /// <summary>
    /// 公立日期
    /// </summary>
    private int Day;
    /// <summary>
    /// 农历 年
    /// </summary>
    private String LunarYear;
    /// <summary>
    /// 生肖年
    /// </summary>
    private String ChZodiac;
    /// <summary>
    /// 农历 月
    /// </summary>
    private String LunarMonth;

    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date date) {
        Date = date;
    }

    public int getYear() {
        return Year;
    }

    public void setYear(int year) {
        Year = year;
    }

    public int getMonth() {
        return Month;
    }

    public void setMonth(int month) {
        Month = month;
    }

    public int getDay() {
        return Day;
    }

    public void setDay(int day) {
        Day = day;
    }

    public String getLunarYear() {
        return LunarYear;
    }

    public void setLunarYear(String lunarYear) {
        LunarYear = lunarYear;
    }

    public String getChZodiac() {
        return ChZodiac;
    }

    public void setChZodiac(String chZodiac) {
        ChZodiac = chZodiac;
    }

    public String getLunarMonth() {
        return LunarMonth;
    }

    public void setLunarMonth(String lunarMonth) {
        LunarMonth = lunarMonth;
    }

    public String getLunarDay() {
        return LunarDay;
    }

    public void setLunarDay(String lunarDay) {
        LunarDay = lunarDay;
    }

    public String getWeek() {
        return Week;
    }

    public void setWeek(String week) {
        Week = week;
    }

    public String getFestival() {
        return Festival;
    }

    public void setFestival(String festival) {
        Festival = festival;
    }

    public String getCons() {
        return Cons;
    }

    public void setCons(String cons) {
        Cons = cons;
    }

    /// <summary>
    /// 农历 日
    /// </summary>
    private String LunarDay;
    /// <summary>
    /// 星期
    /// </summary>
    private String Week;
    /// <summary>
    /// 节日
    /// </summary>
    private String Festival;
    /// <summary>
    /// 星座
    /// </summary>
    private String Cons;
}