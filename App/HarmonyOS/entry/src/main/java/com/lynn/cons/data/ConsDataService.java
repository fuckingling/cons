package com.lynn.cons.data;

import com.lynn.cons.common.DateHelper;
import com.lynn.cons.common.OrmHelper;
import com.lynn.cons.model.Calend;
import com.lynn.cons.model.ConsFortune;
import com.lynn.cons.model.TaskInfo;
import com.lynn.cons.model.User;
import com.lynn.cons.model.em.E_Cons;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ConsDataService {
    private OrmHelper helper;
    private static ConsDataService instance;

    public ConsDataService() {
        helper = OrmHelper.getInstance();
    }

    public static ConsDataService getInstance() {
        if (instance == null) {
            synchronized (ConsDataService.class) {
                if (instance == null) {
                    instance = new ConsDataService();
                }
            }
        }
        return instance;
    }

    /**
     * 添加任务
     *
     * @param title
     * @param content
     * @param tm
     * @return
     * @throws Exception
     */
    public TaskInfo addTask(String title, String content, Date tm) throws Exception {
        if (title.isEmpty()) {
            throw new Exception("标题不能为空");
        }
        if (content.isEmpty()) {
            throw new Exception("内容不能为空");
        }
        TaskInfo task = new TaskInfo();
        task.setTM(tm);
        task.setContent(content);
        task.setTitle(title);
        task.setFinish(0);
        task.setCreatTM(new Date());
        if (helper.insert(task)) {
            return task;
        }
        throw new Exception("添加失败");
    }

    /**
     * 根据日期获取任务
     *
     * @param tm
     * @return
     */
    public List<TaskInfo> getTaskByTM(Date tm) {
        return helper.query(x -> x.equalTo("TM", tm), TaskInfo.class).stream().collect(Collectors.toList());
    }

    /**
     * 获取一周内的任务
     *
     * @return
     */
    public List<TaskInfo> getTaskInWeek() {
        String format = "yyyy-MM-dd 00:00:00";
        Date date = DateHelper.getDate(DateHelper.getWeekStart(format), format);
        Date end = DateHelper.getDate(DateHelper.getWeekEnd(format), format);
        return helper.query(x -> {
            x.between("TM", date, end).or().equalTo("Finish", 0);
        }, TaskInfo.class).stream().collect(Collectors.toList());
    }

    /**
     * 完成任务
     *
     * @param id
     * @return
     */
    public boolean finishTask(int id) {
        TaskInfo taskInfo = helper.query(x -> x.equalTo("Id", id), TaskInfo.class).stream().findFirst().orElse(null);
        if (taskInfo == null) return false;
        taskInfo.setFinish(1);
        taskInfo.setTM(new Date());
        return helper.update(taskInfo);
    }

    /**
     * 根据ID删除任务
     *
     * @param id
     * @return
     */
    public boolean deleteTask(int id) {
        TaskInfo taskInfo = helper.query(x -> x.equalTo("Id", id), TaskInfo.class).stream().findFirst().orElse(null);
        if (taskInfo == null) return false;
        return helper.detete(taskInfo);
    }

    /**
     * 设置用户星座
     *
     * @param conCode
     * @return
     */
    public Boolean updateUserCon(int conCode) {
        User user = helper.query(x -> x.equalTo("Id", 1), User.class).stream().findFirst().orElse(null);
        String conName = E_Cons.get(conCode);
        if (user == null) {
            user = new User();
            user.setId(1);
            user.setConCode(conCode);
            user.setConName(conName);
            return helper.insert(user);
        }
        user.setConName(conName);
        user.setConCode(conCode);
        return helper.update(user);
    }

    /**
     * 获取用户设置
     *
     * @return
     */
    public E_Cons getUserCon() {
        User user = helper.query(x -> x.equalTo("Id", 1), User.class).stream().findFirst().orElse(null);
        if (user == null) {
            user = new User();
            user.setId(1);
            String conName = E_Cons.get(1);
            user.setConCode(1);
            user.setConName(conName);
            return E_Cons.valueOf(conName);
        }
        return E_Cons.valueOf(user.getConName());
    }

    /**
     * 插入日期到本地库
     *
     * @param calends
     */
    public void insertCalend(ArrayList<Calend> calends) {
        for (Calend calend : calends) {
            try {
                helper.insert(calend);
            } catch (Exception e) {
                continue;
            }
        }
    }

    /**
     * 获取今天
     *
     * @return
     */
    public Calend getToday() {
        int code = Integer.parseInt(DateHelper.getToday("yyyyMMdd"));
        return helper.query(x -> x.equalTo("Code", code), Calend.class).stream().findFirst().orElse(null);
    }

    /**
     * 更新或者插入今日(最新)运势
     *
     * @param consFortune
     */
    public void insertOrUpdateConsFortune(ConsFortune consFortune) {
        ConsFortune con = helper.query(x -> x.equalTo("Code", consFortune.Code), ConsFortune.class).stream().findFirst().orElse(null);
        if (con == null) {
            helper.insert(consFortune);
            return;
        }
        con = consFortune;
        helper.update(con);
    }

    /**
     * 获取今日运势
     *
     * @param conCode
     * @return
     */
    public ConsFortune getConsFortune(int conCode) {
        String con = String.valueOf(conCode);
        if (conCode < 10) {
            con = "0" + conCode;
        }
        int code = Integer.parseInt(DateHelper.getToday("yyyyMMdd") + con);
        ConsFortune fortune = helper.query(x -> x.equalTo("Code", code), ConsFortune.class).stream().findFirst().orElse(null);
        if (fortune == null) {
            fortune = helper.query(x -> x.equalTo("Code", code - 100), ConsFortune.class).stream().findFirst().orElse(null);
        }
        return fortune;
    }
}
