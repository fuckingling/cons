package com.lynn.cons.common.net;

import ohos.utils.zson.ZSONObject;

public class Result {
    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String msg) {
        Msg = msg;
    }

    public ZSONObject getData() {
        return Data;
    }

    public void setData(ZSONObject data) {
        Data = data;
    }

    private int Code;
    private String Msg;
    private ZSONObject Data;
}
