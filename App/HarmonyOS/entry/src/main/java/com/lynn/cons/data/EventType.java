package com.lynn.cons.data;

import java.util.Arrays;

public enum EventType {
    /**
     * 添加任务
     */
    ADD_TASK(1001),
    /**
     * 获取5天内的任务
     */
    GET_TASK_IN_WEEK(1002),
    /**
     * 获取任务
     */
    GET_TASK_BY_TM(1003),
    /**
     * 删除任务
     */
    DELETE_TASK(1004),
    /**
     * 完成任务
     */
    FINISH_Task(1005),
    /**
     * 获取用户设置的星座
     */
    GET_USER_CON(1006),
    /**
     * 设置用户星座
     */
    UPDATE_USER_CON(1007);

    EventType(int code) {
        this.Index = code;
    }

    public int getIndex() {
        return Index;
    }

    public void setIndex(int index) {
        Index = index;
    }

    public static EventType get(int index) {
        return Arrays.stream(EventType.values()).filter(x -> x.Index == index).findFirst().get();
    }

    private int Index;
}