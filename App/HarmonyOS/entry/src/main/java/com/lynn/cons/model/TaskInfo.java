package com.lynn.cons.model;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.Index;
import ohos.data.orm.annotation.PrimaryKey;

import java.util.Date;

@Entity(tableName = "TaskInfo", ignoredColumns = "ignoreColumn",
        indices = {@Index(value = {"TM"}, name = "tm_index")})
public class TaskInfo  extends OrmObject {
    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    @PrimaryKey(autoGenerate = true)
    private Integer Id;
    /**
     * 标题
     */
    private String Title;
    /**
     * 内容
     */
    private String Content;



    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public Date getTM() {
        return TM;
    }

    public void setTM(Date TM) {
        this.TM = TM;
    }

    public int getFinish() {
        return Finish;
    }

    public void setFinish(int finish) {
        Finish = finish;
    }

    public Date getCreatTM() {
        return CreatTM;
    }

    public void setCreatTM(Date creatTM) {
        CreatTM = creatTM;
    }

    /**
     * 计划时间
     */
    private Date TM;
    /**
     * 是否已完成 0未完成 1已完成
     */
    private int Finish;
    /**
     * 添加时间
     */
    private Date CreatTM;
}
