package com.lynn.cons.widget.conscard;

import com.lynn.cons.common.DateHelper;
import com.lynn.cons.data.CloudDataService;
import com.lynn.cons.data.ConsDataService;
import com.lynn.cons.model.Calend;
import com.lynn.cons.model.ConsFortune;
import com.lynn.cons.model.em.E_Cons;
import com.lynn.cons.model.em.E_Week;
import com.lynn.cons.widget.controller.FormController;
import ohos.aafwk.ability.*;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.global.icu.util.Calendar;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.zson.ZSONObject;


public class ConsCardImpl extends FormController {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.DEBUG, 0x0, ConsCardImpl.class.getName());
    private static final int DIMENSION_2X4 = 3;

    public ConsCardImpl(Long formId, Context context, String formName, Integer dimension) {
        super(formId, context, formName, dimension);
    }

    @Override
    public ProviderFormInfo bindFormData() {
        ProviderFormInfo providerFormInfo = new ProviderFormInfo();
        providerFormInfo.setJsBindingData(formBindingData());
        return providerFormInfo;
    }

    private FormBindingData formBindingData() {
        ConsDataService service = ConsDataService.getInstance();
        //获取当前用户星座
        E_Cons user = service.getUserCon();
        /** 获取今日运势*/
        ConsFortune fortune = service.getConsFortune(user.getIndex());
        /** 获取今日日历*/
        Calend calend = service.getToday();
        ZSONObject zsonObject = new ZSONObject();

        zsonObject.put("loading", calend == null);
        if (fortune != null) {
            zsonObject.put("friend", "速配" + fortune.getFriend());
            zsonObject.put("score", "今天" + fortune.getAll() + "分");
            zsonObject.put("number", "幸运数字" + fortune.getNumber());
            zsonObject.put("color", "适合" + fortune.getColor());
        } else {
            CloudDataService.getInstance().getConsFortune(user.getIndex(), 0, res -> {
            });
            CloudDataService.getInstance().getConsFortune(user.getIndex(), 1, res -> {
            });
        }
        zsonObject.put("con", user.name());
        zsonObject.put("conDes", user.getStart() + "-" + user.getEnd());
        zsonObject.put("bg", user.getImage());
        Calendar cal = Calendar.getInstance();
        int week = cal.get(java.util.Calendar.DAY_OF_WEEK)-1;
        zsonObject.put("month", DateHelper.geFormat(cal.getTime(), "yyyy年MM月") + " " + E_Week.get(week));
        zsonObject.put("day", cal.get(java.util.Calendar.DATE));
        zsonObject.put("week", E_Week.get(week));
        if (calend != null) {
            zsonObject.put("lunar", calend.getLunarMonth() + " " + calend.getLunarDay());
        } else {
            zsonObject.put("lunar", "");
            CloudDataService.getInstance().getCalendarByDays(5, res -> {
            });
        }
        return new FormBindingData(zsonObject);
    }

    @Override
    public void updateFormData(Object... vars) {
        HiLog.info(TAG, "update form data timing, default 30 minutes");
        try {
            ((Ability) context).updateForm(formId, formBindingData());
        } catch (FormException e) {
            e.printStackTrace();
            HiLog.info(TAG, "更新卡片失败");
        }
    }

    @Override
    public void onTriggerFormEvent(long formId, String message) {
        HiLog.info(TAG, "handle card click event.");
    }

    @Override
    public Class<? extends AbilitySlice> getRoutePageSlice(Intent intent) {
        HiLog.info(TAG, "get the default page to route when you click card.");
        return null;
    }
}
