package com.lynn.cons.model.em;

import java.util.Arrays;

public enum E_Week {
    周日(0),
    周一(1),
    周二(2),
    周三(3),
    周四(4),
    周五(5),
    周六(6);
    public int getIndex() {
        return Index;
    }

    public void setIndex(int index) {
        Index = index;
    }
    private int Index;

    private E_Week(int index) {
        this.Index = index;
    }
    public static String get(int index) {
        return Arrays.stream(E_Week.values()).filter(x -> x.Index == index).findFirst().get().name();
    }
}
