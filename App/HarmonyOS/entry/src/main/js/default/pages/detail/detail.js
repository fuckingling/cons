import router from '@system.router';
import Api from '../../common/utils/apis.js'
import jsFa from '../../common/utils/jsfa.js';
import prompt from '@system.prompt';
import Icons from '../../common/utils/starIcons.config.js'
export default {
    data: {
        user:{
            ConsCode:'',
            ConsName:'巨蟹座',
            ConsDate:''
        },
        barList:[
            {
                name:'',
                icon:'/common/images/icons/icon-back.png',
                iconStyle:{width:'28px',height:'28px'},
                event:'back',
            },
            {
                name:'',
                icon:'/common/images/icons/icon-check.png',
                iconStyle:{width:'30px',height:'27px'},
                event:'handleChooseConst'
            }
        ],
        menuList:[
            {name:'今天',code:'today',type:'day'},
            {name:'明天',code:'tomorrow',type:'day'},
            {name:'本周',code:'week',type:'days'},
            {name:'本月',code:'month',type:'days'}
        ],
        menuActive:'today',
        menuType:'day',
        fortune:'巨蟹座今天在公司当中有比较强的上进心，工作当中的问题也会一一找出来，努力解决掉。压力虽然不小，但是也可以转化成相当的动力，督促自己前行。',
        match:{name:'天蝎座',icon:Icons['双鱼座']},
        content:[
            {
                name:'财富',icon:'/common/images/icons/icon-money.png',iconClass:'money',value:'88'
            },
            {
                name:'工作',icon:'/common/images/icons/icon-tie.png',iconClass:'work',value:'88'
            },
            {
                name:'健康',icon:'/common/images/icons/icon-heartline.png',iconClass:'health',value:'88'
            },
            {
                name:'爱情',icon:'/common/images/icons/icon-heart.png',iconClass:'love',value:'88'
            },
            {
                name:'幸运颜色',icon:'/common/images/icons/icon-lucky.png',iconClass:'color',value:'墨绿色'
            },
        ],
        futureData:[
            {
                name:'爱情',
                icon:'/common/images/icons/icon-heart.png',
                iconClass:'love',
                text:'恋爱中的巨蟹，本周在感情上面的问题也会更加敏感，想要牢牢的把握彼此关系，但对方的回应不会太积极，给感情中带来很多的不投机；单身的巨蟹，本周有机会通过相亲结识物质条件不错的对象，可以努力把握。'
            },
            {
                name:'工作',
                icon:'/common/images/icons/icon-tie.png',
                iconClass:'work',
                text:'事业上，巨蟹本周做事会更有分寸，懂得如何理智的处理问题，语言方面的表达也会让人更容易接受，会为自己迎来更好的合作关系；学习上，巨蟹本周考试运势不错，有益于开发自己的脑力，学习动力十足。'
            },
            {
                name:'财富',
                icon:'/common/images/icons/icon-money.png',
                iconClass:'money',
                text:'巨蟹本周财富运势较为平稳，能够更好地应对此前的负债问题，但收入上不会有太大突破，还需严谨的把控支出。'
            },
            {
                name:'健康',
                icon:'/common/images/icons/icon-heartline.png',
                iconClass:'health',
                text:'巨蟹本周在健康方面容易出现牙齿肿胀或酸痛的情况，饮食上需要以清淡为主，及时刷牙保持口腔的清洁。'
            },
//            {
//                name:'整体运势',
//                icon:'/common/images/icons/icon-heart.png',
//                text:'对于绝大部分白羊而言，2020都是比较艰难的一年，尽管付出了很多努力，但获得的收获却非常不尽如人意。步入2021，落座于双鱼和水瓶座的木星在白羊们的星图之上高照，你会拥有许多丰富多彩的创意和想法，也能够一步步地将它们落实到位，生活中遇到的挫折会在木星这颗吉星的帮助下顺利化解。但是，火星的火热能量却会让你在不同的领域变得有些急性子，既不容易有条理地做好事情，也比较容易恼羞成怒甚至发生争执。不妨试着让自己宽容一些，抱着“退一步海阔天空”的心态度过这一年吧。白羊座今年可佩戴一个星盘保岁吉宏项链作为全年的幸运护身符饰物，银币铸造的船舵星符可提升白羊们的能量指数，寓意今年信心十足、目标明确、勇往直前！'
//            }
        ],
        showDialog:false,
        baseData:[]

    },
    onInit(){
        this.getBaseInfo()
    },
//    获取所有星座基础信息
    getBaseInfo(){
        Api.getConstellations().then(data => {
            this.baseData = data
            this.getData()
        }).catch(err => {
            prompt.showToast({
                message: '获取星座基础信息出错啦! ' + JSON.stringify(err),
                duration: 30000,
            });
        });
    },
//    获取用户当前星座，更新用户星座code
    getData() {
        jsFa.GetUserCon().then(res => {
            this.user.ConsName = res
            this.updateUserConsCode()
        }).catch(err => {
            prompt.showToast({
                message: '网络错误，获取用户星座失败' + JSON.stringify(err),
                duration: 30000,
            });
        })
    },
//    更新当前用户星座code
    updateUserConsCode(){
        const consMap = {}
        this.baseData.map(item => {
            const start = formatDate(item.startDate.toString())
            const end = formatDate(item.endDate.toString())
            consMap[item.name] = {code:item.code,date:`${start}-${end}`}
        })
        function formatDate(dateString){
            if(dateString.length === 3){
                return `${dateString[0]}月${dateString[1]}${dateString[2]}日`
            }
            if(dateString.length === 4){
                return `${dateString[0]}${dateString[1]}月${dateString[2]}${dateString[3]}日`
            }
        }
        this.user.ConsCode = consMap[this.user.ConsName].code
        this.user.ConsDate = consMap[this.user.ConsName].date
        this.getTypesData()
    },
//    获取星座今日运势
    getTodayConsData(){
        Api.getTodayConsFortune({e_Cons:this.user.ConsCode}).then(res => {
            const data = res
            this.content[0].value = data.money
            this.content[1].value = data.work
            this.content[2].value = data.health
            this.content[3].value = data.love
            this.content[4].value = data.color
            this.fortune = data.summary
            this.match.name = data.friend
            this.match.icon = Icons[data.friend]
        }).catch(err => {
            prompt.showToast({
                message: '网络错误，获取数据失败 ' + JSON.stringify(err),
                duration: 30000,
            });
        })
    },
//    获取星座明日运势
    getTomorrowConsData(){
        Api.getTomorrowConsFortune({e_Cons:this.user.ConsCode}).then(res => {
            const data = res
            this.content[0].value = data.money
            this.content[1].value = data.work
            this.content[2].value = data.health
            this.content[3].value = data.love
            this.content[4].value = data.color
            this.fortune = data.summary
            this.match.name = data.friend
            this.match.icon = Icons[data.friend]
        }).catch(err => {
            prompt.showToast({
                message: '网络错误，获取数据失败 ' + JSON.stringify(err),
                duration: 30000,
            });
        })
    },
//    获取星座本周、本月、本年运势
    getWeekConsData(type){
        Api.GetFortuneRecord({e_Cons:this.user.ConsCode,type}).then(res => {
            const data = res
            this.futureData[0].text = data.love
            this.futureData[1].text = data.work
            this.futureData[2].text = data.money
            this.futureData[3].text = data.health
            this.fortune = data.summary
        }).catch(err => {
            prompt.showToast({
                message: '网络错误，获取数据失败 ' + JSON.stringify(err),
                duration: 30000,
            });
        })
    },
//    今天、明天、本周、本月按钮点击
    menuClick(item){
        this.menuActive = item.code
        if(this.menuType === item.type){
            this.$element('list').scrollTop({smooth: true})
        }
        this.menuType = item.type
        this.getTypesData()
    },
    getTypesData(){
        switch (this.menuActive) {
            case 'today':this.getTodayConsData();break;
            case 'tomorrow':this.getTomorrowConsData();break;
            case 'week':this.getWeekConsData(0);break;
            case 'month':this.getWeekConsData(1);break;
        }
    },
//    导航栏按钮点击
    handleBarClick(e){
        const eventName = e.detail.eventName
        this[eventName]()
    },
//    返回上一页
    back(){
        router.back()
    },
//    打开星座选择组件
    handleChooseConst(){
        this.showDialog = true
    },
//    关闭星座弹窗组件
    handleClose(){
        this.showDialog = false
    },
//    处理星座选择事件
    handleChooseSubmit(e){
        jsFa.UpdateUserCon(e.detail.code).then(res => {
            this.getData()
            this.handleClose()
        }).catch(err => {
            prompt.showToast({
                message: '设置星座失败！' + JSON.stringify(err),
                duration: 30000,
            });
        })
    },
}
