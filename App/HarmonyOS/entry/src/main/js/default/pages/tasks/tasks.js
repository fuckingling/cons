import router from '@system.router';
import time from '../../common/utils/timehelper.js'
import jsFa from '../../common/utils/jsfa.js';
import prompt from '@system.prompt';
// @ts-ignore
import mock from './mock.json'

export default {
    data: {
        barList: [
            {
                name: '任务',
                icon: '/common/images/icons/icon-back.png',
                iconStyle:{width:'28px',height:'28px'},
                event: 'back',
            },
//            {
//                name: '',
//                icon: '/common/images/icons/icon-check.png',
//                iconStyle:{width:'30px',height:'27px'},
//                event: ''
//            }
        ],
        todoList:[
//            {
//                name:'签签雀割',
//                time:'明天',
//                state:''
//            },
//            {
//                name:'签签雀割',
//                time:'明天',
//                state:'已完成'
//            }
        ],
        selectTime:null,
        date:''
    },
    onInit() {
        this.date = time.getTodayString()
        this.getTaskByDate()
    },
//    顶部bar按钮点击事件
    handleBarClick(e) {
        const eventName = e.detail.eventName
        this[eventName]()
    },
//    返回上一页
    back() {
        router.back()
    },
//    获取当前日期任务列表
    getTaskByDate(){
        jsFa.GetTaskByTM(this.date).then(res => {
//            prompt.showToast({
//                message: '当前任务' + JSON.stringify(res),
//                duration: 30000,
//            });
            this.todoList = res.sort((a,b)=>{
                return a.finish-b.finish;
            }).map(item => {
                return {
                    id:item.id,
                    name:item.title,
                    state:item.finish,
                    time:time.dateTaskFormat(item.tM),
                    finishTime: time.dateTaskFormat(item.tM),
                    transX: 0,
                    startX: 0
                }
            })

        }).catch(err => {
            prompt.showToast({
                message: '网络错误，获取任务失败' + JSON.stringify(err),
                duration: 30000,
            });
        })
    },
//    日期选择
    select(e){
        console.debug(JSON.stringify(e.detail.day))
        const date = e.detail.day
        this.date =`${date.year}-${formate(date.month)}-${formate(date.day)}`
        this.getTaskByDate()
        function formate(date){
            return date > 9 ? date : '0'+date
        }
    },
//    删除任务
    handleDeleteTask(e){
        const id = e.detail.item.id
        jsFa.DeleteTask(id).then(res => {
            this.getTaskByDate()
        })
    },
//    任务完成标记
    handleFinishTask(e){
        const id = e.detail.item.id
        jsFa.FinishTask(id).then(res => {
            this.getTaskByDate()
        })
    }
}
