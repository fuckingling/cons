//import http from '../utils/httphelper.js'
import http from '../utils/fetchhelper.js'
const base_url = "http://cons.api.lynnce.com:10000"
//获取日历数据
const GetCalendar = "/api/Calend/GetCalendar"
//获取星座基本信息
const GetConstellations = "/api/Cons/GetConstellations"
//获取今日运势
const getTodayConsFortune = "/api/Cons/getTodayConsFortune"
//获取明日运势
const getTomorrowConsFortune = "/api/Cons/getTomorrowConsFortune"
//获取周、月、年运势
const GetFortuneRecord = "/api/Cons/GetFortuneRecord"
//根据日期获取农历信息
const GetLunarZodiac = "/api/Lunar/GetLunarZodiac"

export default {
    /**
     * 获取星座基本信息
     */
    getConstellations(data)
    {
        var url = base_url + GetConstellations;
        return http.get(url, data);
    },
    /**
     * 获取今日运势
     */
    getTodayConsFortune(data)
    {
        var url = base_url + getTodayConsFortune;
        return http.get(url, data);
    },
    /**
     * 获取明日运势
     */
    getTomorrowConsFortune(data)
    {
        var url = base_url + getTomorrowConsFortune;
        return http.get(url, data);
    },
    /**
     * 获取周、月、年运势
     */
    GetFortuneRecord(data)
    {
        var url = base_url + GetFortuneRecord;
        return http.get(url, data);
    },
    /**
     * 根据日期获取农历信息
     */
    GetLunarZodiac(data)
    {
        var url = base_url + GetLunarZodiac;
        return http.get(url, data);
    },
}