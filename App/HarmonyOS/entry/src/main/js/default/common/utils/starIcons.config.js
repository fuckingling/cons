export default
    {
        '白羊座':{url:'/common/images/starts/byz.png',width:'450px',height:'280px',opacity:0.5,iconW:'210px',iconH:'130px'},
        '金牛座':{url:'/common/images/starts/jnz.png',width:'414px',height:'403px',opacity:0.5,iconW:'140px',iconH:'130px'},
        '双子座':{url:'/common/images/starts/szz.png',width:'405px',height:'288px',opacity:0.4,iconW:'150px',iconH:'130px'},
        '巨蟹座':{url:'/common/images/starts/jxz.png',width:'445px',height:'358px',opacity:1,iconW:'140px',iconH:'130px'},
        '狮子座':{url:'/common/images/starts/sz.png',width:'380px',height:'350px',opacity:0.5,iconW:'140px',iconH:'130px'},
        '处女座':{url:'/common/images/starts/cnz.png',width:'360px',height:'308px',opacity:0.35,iconW:'150px',iconH:'130px'},
        '天秤座':{url:'/common/images/starts/tcz.png',width:'380px',height:'326px',opacity:0.35,iconW:'150px',iconH:'130px'},
        '天蝎座':{url:'/common/images/starts/txz.png',width:'460px',height:'400px',opacity:0.5,iconW:'140px',iconH:'130px'},
        '射手座':{url:'/common/images/starts/ssz.png',width:'366px',height:'390px',opacity:0.5,iconW:'120px',iconH:'130px'},
        '摩羯座':{url:'/common/images/starts/mjz.png',width:'444px',height:'367px',opacity:0.35,iconW:'125px',iconH:'130px'},
        '水瓶座':{url:'/common/images/starts/spz.png',width:'409px',height:'443px',opacity:0.35,iconW:'120px',iconH:'130px'},
        '双鱼座':{url:'/common/images/starts/syz.png',width:'458px',height:'305px',opacity:0.35,iconW:'160px',iconH:'120px'},
    }
