export default{
    data:{
        touchStartPos:{},
        touchEndPos:{},
        startX:10,
        startY:10,
        transX:10,
        transY:10
    },
    handleTouchstart(e){
        this.touchStartPos.x = e.touches[0].globalX
        this.touchStartPos.y = e.touches[0].globalY
    },
    handleTouchmove(e){
        const dx = e.changedTouches[0].globalX - this.touchStartPos.x
        const dy = e.changedTouches[0].globalY - this.touchStartPos.y
        this.transX = this.startX - dx
        this.transY = this.startY - dy
    },
    handleTouchend(e){
        console.log('touch end position')
        this.transX = 10
        this.transY = 10
        this.startX = this.transX
        this.startY = this.transY
    }
}