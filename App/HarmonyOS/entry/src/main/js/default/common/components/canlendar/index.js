import router from '@system.router';
import time from '../../utils/timehelper.js'
// @ts-ignore
import mock from './mock.json'

export default {
    props: ['monthView', 'slider', 'showTips'],
    data: {
        //        monthView: true, //全月视图
        slider: false, //上下滑动，默认禁用 不太好用
        down: false, //记录当前手势是上还是下,
        smallHeight: 100,
        largeHeight: 522,
        midDays: {},
        preDays: {},
        nextDays: {},
        views: [{}, {}, {}],
        select: {
            code: 20210101,
            year: 2021,
            month: 1,
            day: 1,
            week: 5
        },
        today: {
            code: 20210101,
            year: 2021,
            month: 1,
            day: 1,
            week: 5
        },
        month: {},
        count: 3,
        defaultIndex: 0,
        height: 82
    },
    onInit() {
        if (this.monthView === "true") {
            this.monthView = true
        } else {
            this.monthView = false
        }
        if (this.slider === "true") {
            this.slider = true
        } else {
            this.slider = false
        }
        if (this.showTips === "true") {
            this.showTips = true
        } else {
            this.showTips = false
        }
        this.height = this.monthView ? this.largeHeight : this.smallHeight;
        var date = new Date();
        this.today.code = parseInt(time.dateFormat("yyyyMMdd", date));
        this.today.year = date.getFullYear();
        this.today.month = date.getMonth() + 1;
        this.today.day = date.getDate();
        this.today.week = date.getDay();
        this.select = JSON.parse(JSON.stringify(this.today))
        this.monthChange({
            index: this.defaultIndex
        });
    },
    calMonth(year, month, day) {
        //        console.debug(`year:${year}, month:${month}, day:${day}`)
        //月视图必须从1号开始 周视图从周一开始
        day = this.monthView ? 1 : day;
        var preDate = new Date(year, month - 1, day);
        //向上查找周一
        var n = 1 - (preDate.getDay() == 0 ? 7 : preDate.getDay());
        preDate.setDate(preDate.getDate() + n);

        var start = time.dateFormat("yyyyMMdd", preDate);
        //一个面板的所需日期集合 7*6规则 和1*7规则
        var days = this.monthView ? 6 * 7 : 1 * 7;
        preDate.setDate(preDate.getDate() + days); //确保最后一周能覆盖
        var end = time.dateFormat("yyyyMMdd", preDate);
        //数据侧
        var months = mock.filter(x => x.code >= start && x.code < end);
        //确保日期顺序正确
        months.sort(function (a, b) {
            return a.code - b.code
        });
        //组装成二维数组
        var res = [];
        var len = this.monthView ? 6 : 1;
        for (var i = 0;i < len; i++) {
            res[i] = months.slice(parseInt(i) * 7, (parseInt(i) + 1) * 7)
        }
        return {
            res,
            year,
            month,
            day
        }
    },
    monthChange(val) {
        var index = parseInt(val.index);
        var m = [this.preDays, this.midDays, this.nextDays];
        if (m[index] === null || m[index] === "undefined" || JSON.stringify(m[index]) == "{}") {
            m[index] = this.calMonth(this.select.year, this.select.month, this.select.day);
        }
        //月视图
        if (this.monthView) {
            if (m[index].year != this.select.year || m[index].month != this.select.month) {
                var date = new Date(m[index].year, m[index].month - 1, m[index].day);
                this.select.code = parseInt(time.dateFormat("yyyyMMdd", date));
                this.select.year = date.getFullYear();
                this.select.month = date.getMonth() + 1;
                this.select.day = date.getDate();
                this.select.week = date.getDay();
            }
            if (m[index].year == this.today.year && m[index].month == this.today.month) {
                this.select = JSON.parse(JSON.stringify(this.today))
            }
        }
        //周视图
        if (!this.monthView) {
            if (m[index].year != this.today.year || m[index].month != this.today.month || m[index].day != this.today.day) {
                var date = new Date(m[index].year, m[index].month - 1, m[index].day);
                this.select.code = parseInt(time.dateFormat("yyyyMMdd", date));
                this.select.year = date.getFullYear();
                this.select.month = date.getMonth() + 1;
                this.select.day = date.getDate();
                this.select.week = date.getDay();
            } else {
                this.select = JSON.parse(JSON.stringify(this.today))
            }
        }
        for (var i = 0;i < 3; i++) {
            if (i == index)continue;
            var date = new Date(this.select.year, this.select.month - 1, this.select.day);
            //变更数组中其他月份的数据
            var mdy = i - index;
            if (index == 0 && i == 2) {
                mdy = -1;
            }
            if (index == 2 && i == 0) {
                mdy = 1;
            }
            if (this.monthView) {
                if (mdy < 0) {
                    date.setMonth(date.getMonth() - 1);
                } else {
                    date.setMonth(date.getMonth() + 1);
                }
            } else {
                if (mdy < 0) {
                    date.setDate(date.getDate() - 7);
                } else {
                    date.setDate(date.getDate() + 7);
                }
            }
            m[i] = this.calMonth(date.getFullYear(), date.getMonth() + 1, date.getDate());
        }
        this.preDays = m[0];
        this.nextDays = m[2];
        this.midDays = m[1];
        this.views = [this.preDays, this.midDays, this.nextDays];
        this.$emit('select', {
            day: this.select
        })
    },
    touchmove(msg) {
        var y = msg.touches[0].localY;
        var h = this.monthView ? this.largeHeight : this.smallHeight;
        var height = h + y;
        this.down = height > this.largeHeight / 2;
        if (height <= this.smallHeight) {
            height = this.smallHeight;
        }
        if (height >= this.largeHeight) {
            height = this.largeHeight;
        }
        this.height = height;
    },
    touchend() {
        if (this.down) {
            this.height = this.largeHeight;
            this.monthView = true;
        } else {
            this.height = this.smallHeight;
            this.monthView = false;
        }
        this.preDays = {};
        this.midDays = {};
        this.nextDays = {};
        this.monthChange({
            index: this.defaultIndex
        });
    },
    onSelect(day) {
//        if (day.month != this.select.month || day.year != this.select.year) {
//            return;
//        }
        this.select.code = day.code;
        this.select.year = day.year;
        this.select.month = day.month;
        this.select.day = day.day;
        this.select.week = day.week;
        this.$emit('select', {
            day: this.select
        })
    }
}
