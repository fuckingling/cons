export default {
    dateFormat(fmt, date) {
        let ret;
        const opt = {
            "y+": date.getFullYear().toString(), // 年
            "M+": (date.getMonth() + 1).toString(), // 月
            "d+": date.getDate().toString(), // 日
            "H+": date.getHours().toString(), // 时
            "m+": date.getMinutes().toString(), // 分
            "s+": date.getSeconds().toString() // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
        };
        for (let k in opt) {
            ret = new RegExp("(" + k + ")").exec(fmt);
            if (ret) {
                fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
            }
            ;
        }
        ;
        return fmt;
    },
    dateTaskFormat(date){
        const current = new Date()
        const targetDate = new Date(date)
        const cur = {
            'year':current.getFullYear(),
            'month':current.getMonth()+1,
            'day':current.getDate()
        }
        const target = {
            'year':targetDate.getFullYear(),
            'month':targetDate.getMonth()+1,
            'day':targetDate.getDate()
        }
        if(target.year === cur.year){
            if(target.month === cur.month){
                if(target.day === cur.day){
                    return '今天'
                }else if(target.day-cur.day === 1){
                    return '明天'
                }else if(target.day-cur.day === 2){
                    return '后天'
                }else if(target.day-cur.day === -1){
                    return '昨天'
                }
                else {
                    return `${target.year}-${target.month}-${target.day}`
                }
            }else{
                return `${target.year}-${target.month}-${target.day}`
            }
        }else{
            return `${target.year}-${target.month}-${target.day}`
        }
    },
    getTodayString(){
        const date = new Date()
        const target = {
            'year':date.getFullYear(),
            'month':date.getMonth()+1,
            'day':date.getDate()
        }
        return `${target.year}-${format(target.month)}-${format(target.day)}`
        function format(val){
            return val > 9 ? val : `0${val}`
        }
    }
}