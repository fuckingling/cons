import prompt from '@system.prompt';
// abilityType: 0-Ability; 1-Internal Ability
const ABILITY_TYPE_EXTERNAL = 0;
const ABILITY_TYPE_INTERNAL = 1;
// syncOption(Optional, default sync): 0-Sync; 1-Async
const ACTION_SYNC = 0;
const ACTION_ASYNC = 1;
/**
     * 添加任务
     */
const ADD_TASK = 1001;
/**
     * 获取本周的任务
     */
const GET_TASK_IN_WEEK = 1002;
/**
     * 获取任务
     */
const GET_TASK_BY_TM = 1003;
/**
     * 删除任务
     */
const DELETE_TASK = 1004;
/**
     * 完成任务
     */
const FINISH_Task = 1005;
/**
     * 获取用户设置的星座
     */
const GET_USER_CON=1006;
/**
     * 设置用户星座
     */
const UPDATE_USER_CON=1007;
export default {
    getJavaData: async function(code, actionData) {
        var action = {};
        action.bundleName = "com.lynn.cons";
        action.abilityName = "com.lynn.cons.data.DataServiceAbility";
        action.messageCode = code;
        action.data = actionData;
        action.abilityType = ABILITY_TYPE_EXTERNAL;
        action.syncOption = ACTION_SYNC;

        var result = await FeatureAbility.callAbility(action);
        var ret = JSON.parse(result);
        console.log('result===:' + ret)
        return new Promise(function (resolve, reject) {
            if (ret.code == 200) {
                resolve(ret.data)
            } else {
                reject(ret)
            }
        });
    },
/**
     * 添加任务
     */
    AddTask: async function(title, content, date) {
        return await this.getJavaData(ADD_TASK, {
            date: date,
            title: title,
            content: content
        })
    },
    /**
     * 根据日期获取任务 yyyy-MM-dd
     */
    GetTaskByTM: async function(date) {
        return await this.getJavaData(GET_TASK_BY_TM, {
            date: date,
        })
    },
    /**
     * 获取5天内的任务
     */
    GetTaskIn5Days: async function() {
        return await this.getJavaData(GET_TASK_IN_WEEK, {})
    },
    /**
     * 删除任务
     */
    DeleteTask: async function(id) {
        return await this.getJavaData(DELETE_TASK, {
            id: id,
        })
    },
    /**
     * 完成任务
     */
    FinishTask: async function(id) {
        return await this.getJavaData(FINISH_Task, {
            id: id,
        })
    },
    /**
     * 设置用户星座
     */
    UpdateUserCon: async function(id) {
        return await this.getJavaData(UPDATE_USER_CON, {
            id: id,
        })
    },
    /**
     * 获取用户星座
     */
    GetUserCon: async function() {
        return await this.getJavaData(GET_USER_CON, {
        })
    },
}