import prompt from '@system.prompt';
import jsFa from '../../../common/utils/jsfa.js';
export default {
    data:{
        dateList: [
            {
                name: '今天',
                icon: '',
                index: 0
            },
            {
                name: '明天',
                icon: '',
                index: 1
            },
            {
                name: '后天',
                icon: '',
                index: 2
            },
            {
                name: '选择日期',
                icon: '/common/images/icons/icons-down2.png',
                activeIcon: '/common/images/icons/icon-down.png',
                index: 3
            }
        ],
        dateIndex: 0,
        textAreaValue: '',
        dateString:'',
        showcal:false
    },
    props:['page','date'],
    onInit(){
        if(this.date){
            this.dateString = this.date
        }else{
            this.handleTaskDateChoose(this.dateIndex)
        }
    },
//    弹出新建任务组件
    handleCreate() {
        this.$element('createTask').show()
        this.$element('textarea').focus()
        this.showcal=true
    },
//    任务时间选择
    handleTaskDateChoose(idx) {
        let date = new Date()
        this.dateIndex = idx
        switch (idx) {
            case 0:
            this.dateString = `${date.getFullYear()}-${this.formatDate(date.getMonth()+1)}-${this.formatDate(date.getDate())}`;
            this.dateList[3].name = '选择日期'
            break;
            case 1:
            this.dateString = `${date.getFullYear()}-${this.formatDate(date.getMonth()+1)}-${this.formatDate(date.getDate()+1)}`;
            this.dateList[3].name = '选择日期'
            break;
            case 2:
            this.dateString = `${date.getFullYear()}-${this.formatDate(date.getMonth()+1)}-${this.formatDate(date.getDate() + 2)}`;
            this.dateList[3].name = '选择日期'
            break;
            case 3:
            this.$element('calendar').show()
            break;
        }
    },
    handleTextAreaBlur(e) {

    },
//    任务内容获取
    handleTextAreaChange(e) {
        this.textAreaValue = e.text
        //        prompt.showToast({
        //            message: 'value: ' + e.text + ', lines: ' + e.lines + ', height: ' + e.height,
        //            duration: 3000,
        //        });
    },
//    新建任务
    handleSubmitTask() {
        this.dateString = this.date || this.dateString
        if (!this.textAreaValue) {
            prompt.showToast({
                message: '请先输入任务内容',
                duration: 3000,
            });
            return
        }
        jsFa.AddTask(this.textAreaValue, this.textAreaValue, this.dateString).then(data=>{
            this.$emit('success')
        }).catch(err => {
            prompt.showToast({
                message: '添加任务失败：' + JSON.stringify(err),
                duration: 30000,
            });
        });
        this.showcal=false
        this.$element('createTask').close()
        this.textAreaValue = ''
    },
    select(e) {
        const date = e.detail.day
        this.dateString =`${date.year}-${formate(date.month)}-${formate(date.day)}`
        this.dateList[3].name = this.dateString
        function formate(date){
            return date > 9 ? date : '0'+date
        }
    },
    formatDate(date){
        console.debug(date)
        return date > 9 ? date : '0'+date
    }
}