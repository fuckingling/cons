export default {
    formateObjToParamStr(paramObj) {
        const sdata = [];
        for (let attr in paramObj) {
            sdata.push(`${attr}=${paramObj[attr]}`);
        }
        return sdata.join('&');
    }
}