import prompt from '@system.prompt';
export default {
    data:{
        touchStartPos: {},
        transX:0,
        eventFlag:false,
    },
    handleTouchStart(item,e){
//        if(item.state){
//            return
//        }
        this.touchStartPos.x = e.touches[0].globalX
        item.transX = item.startX
        console.log('touch start position')
    },
    handleTouchMove(item,e){
        if(item.state&&this.transX>0){
            return
        }
        this.transX = e.changedTouches[0].globalX - this.touchStartPos.x
        console.log('touch move position')
//        向左滑动展示操作按钮
        if(item.startX === 0){
//            if(this.transX > 0) return
        }
//        向右滑动隐藏操作按钮
        else{
//            if (this.transX < 0) return
        }
        item.transX = this.transX + item.startX
    },
    handleTouchEnd(item){
        console.log('touch end position')
//        向左滑动展示操作按钮
        if(item.transX <0){
            this.leftMoveEnd(item)
        }else{
            this.rightMoveEnd(item)
        }
        item.startX = item.transX
        this.transX = 0
    },
    leftMoveEnd(item){
        //        向左滑动展示操作按钮
        if(item.transX > -140){
            item.transX = 0
        }else{
            item.transX = -140
            this.handleDelete(item)
        }
    },
    rightMoveEnd(item){
        if(item.transX <140){
            item.transX = 0
        }else{
            item.transX = 140
            this.handleMarkFinish(item)
        }
    },
    handleMarkFinish(item){
        console.log('标记完成按钮被点击了')
//        prompt.showToast({
//            message: '标记完成按钮被点击了',
//            duration: 3000,
//        });
        item.transX = 0
        item.startX = 0
        this.$emit('finish',{item})
    },
    handleDelete(item){
        console.log('删除按钮被点击了')
//        prompt.showToast({
//            message: '删除按钮被点击了',
//            duration: 3000,
//        });
        item.transX = 0
        item.startX = 0
        this.$emit('delete',{item})
    },
    handleMenuClick(item){
        console.log('菜单按钮被点击了')
//        prompt.showToast({
//            message: '菜单按钮被点击了'+item.eventFlag,
//            duration: 3000,
//        });
        if(!item.eventFlag){
            item.transX = -280
            item.startX = -280
        }else{
            item.transX = 0
            item.startX = 0
        }
        item.eventFlag = !item.eventFlag
    }
}