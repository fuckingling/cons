import fetch from '@system.fetch';
import prompt from '@system.prompt';
import obj from '../utils/objecthelper.js'

export default {
    get(url,data){
        const request = new Promise((resolve,reject)=>{
            url = `${url}?${obj.formateObjToParamStr(data)}`;
            fetch.fetch({
                url,
                method: "GET",
                header: {
                    'Content-Type': 'application/json'
                },
                success:(res) => {
                    if(res.code === 200){
                       const data = JSON.parse(res.data).data
                        resolve(data)
                    }else{
                        reject(res.data)
                    }
//                    数据返回格式默认是string ，需要用JSON.parse方法处理为json对象
//                    prompt.showToast({
//                        message: '返回数据：'+res.data,
//                        duration: 30000,
//                    });
                },
                fail:(err) => {
                    console.log(err)
                    reject(err)
                    prompt.showToast({
                        message: '接口错误：'+err,
                        duration: 30000,
                    });
                }
            })
        })
        return request
    }
}