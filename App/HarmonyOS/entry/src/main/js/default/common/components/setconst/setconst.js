import prompt from '@system.prompt';
import Api from '../../utils/apis.js'

export default {
    data: {
        constData: [
            {
                name: '白羊座',
                icon: '/common/images/constImage/byz.png',
                width: '133px',
                height: '84px'
            },
            {
                name: '金牛座',
                icon: '/common/images/constImage/jnz.png',
                width: '103px',
                height: '100px'
            },
            {
                name: '双子座',
                icon: '/common/images/constImage/szzz.png',
                width: '122px',
                height: '100px'
            },
            {
                name: '巨蟹座',
                icon: '/common/images/constImage/jxz.png',
                width: '111px',
                height: '89px'
            },
            {
                name: '狮子座',
                icon: '/common/images/constImage/szz.png',
                width: '111px',
                height: '110px'
            },
            {
                name: '处女座',
                icon: '/common/images/constImage/cnz.png',
                width: '87px',
                height: '113px'
            },
            {
                name: '天秤座',
                icon: '/common/images/constImage/tcz.png',
                width: '142px',
                height: '123px'
            },
            {
                name: '天蝎座',
                icon: '/common/images/constImage/txz.png',
                width: '128px',
                height: '123px'
            },
            {
                name: '射手座',
                icon: '/common/images/constImage/ssz.png',
                width: '91px',
                height: '97px'
            },
            {
                name: '摩羯座',
                icon: '/common/images/constImage/mjz.png',
                width: '110px',
                height: '110px'
            },
            {
                name: '水瓶座',
                icon: '/common/images/constImage/spz.png',
                width: '109px',
                height: '117px'
            },
            {
                name: '双鱼座',
                icon: '/common/images/constImage/syz.png',
                width: '121px',
                height: '83px'
            }
        ],
        translateValue: '-700px',
        datas:[],
    },
    props:['allData'],
    onInit() {
        if(this.allData.length > 0){
            this.updateConsData()
        }else{
//            prompt.showToast({
//                message: '组件没有基础数据',
//                duration: 30000,
//            });
//            this.getConstBaseData.then(() => {
//                this.updateConsData()
//            })

        }
    },
    onAttached() {
//        组件挂载,处理出现动画
        const $main = this.$element('main')
        const $container = this.$element('container')
        const $this = this
        const options = {
            duration: 200,
            easing: 'friction',
            delay: 0,
            fill: 'forwards',
            iterations: 1,
            direction: 'normal',
            begin: 50,
        };
        const frames = [
            {
                transform: {
                    translateX: '0px'
                },
                opacity: 0,
                offset: 0.0
            },
            {
                transform: {
                    translateX: '700px'
                },
                opacity: 1,
                offset: 1.0
            }
        ];
        this.animation = $main.animate(frames, options);

        //handle finish event
        this.animation.onfinish = function () {
            $this.translateValue = '0px';
//            prompt.showToast({
//                message: "The animation is finished."
//            });
        }
        this.animation.play();
    },
    onLayoutReady() {
//        这个方法会在页面渲染时多次触发
    },
    handleClose() {
//        console.log('最外层区域冒泡事件触发')
        this.$emit('close')
    },
    handleChoose(item) {
//        prompt.showToast({
//            message: "您选择了" + item.name+'id:'+item.code
//        });
        this.$emit('choose', {
            name:item.name,
            code:item.code
        })

    },
    updateConsData(){
        const data = this.allData.length ? this.allData : this.datas
        if( data.length === 0){
            prompt.showToast({
                message: '网络错误，请稍后重试',
                duration: 3000,
            });
            return
        }
        const consMap = {}
            data.map(item => {
                consMap[item.name] =item.code
        })
        this.constData.map(item => {
            item.code = consMap[item.name]
        })
//        prompt.showToast({
//            message: JSON.stringify(this.constData),
//            duration: 30000,
//        });
    },
    getConstBaseData(){
        return Api.getConstellations().then(data => {
            this.datas = data
        })
    }
}