// @ts-nocheck
import http from '@ohos.net.http';
import prompt from '@system.prompt';
import obj from '../utils/objecthelper.js'

export default {
    get(url, data) {
        let httpRequest = http.createHttp();
        url = `${url}?${obj.formateObjToParamStr(data)}`;
        let promise = httpRequest.request(url, {
            method: "GET",
            header: {
                'Content-Type': 'application/json'
            }
        });
        promise.then(ret => {
            if (ret.data == 200) {
                resolve(ret.data)
            } else {
                prompt.showToast({
                    message: ret.msg,
                    duration: 30000,
                });
                reject(ret.msg)
            }
        }).catch(err => {
            prompt.showToast({
                message: '请求错误',
                duration: 30000,
            });
            reject(err)
        });
        return promise;
    }
}
